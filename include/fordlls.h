/*
 * Copyright © 2023 Jonathan Leivent
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice (including the
 * next paragraph) shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

// In thread per client session mode, we cannot redirect stderr separately in
// each thread, so instead we redefine stderr and STDERR_FILENO to be
// per_thread variables:

#ifndef FORDLLS_H
#define FORDLLS_H

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <unistd.h>

/** Visibility attribute */
#if defined(__GNUC__) && __GNUC__ >= 4
#define EXPORT __attribute__ ((visibility("default")))
#else
#define EXPORT
#endif

#ifndef START_EXTERN_C
#ifdef __cplusplus
#define START_EXTERN_C extern "C" {
#define END_EXTERN_C }
#else
#define START_EXTERN_C
#define END_EXTERN_C
#endif
#endif

START_EXTERN_C

extern EXPORT __thread FILE *per_thread_stderr;
extern EXPORT FILE *original_stderr;
extern EXPORT __thread int per_thread_STDERR_FILENO;
extern EXPORT int original_STDERR_FILENO;

#ifdef stderr
#undef stderr
#endif
#define stderr per_thread_stderr
#ifdef STDERR_FILENO
#undef STDERR_FILENO
#endif
#define STDERR_FILENO per_thread_STDERR_FILENO


EXPORT struct wd_interface;
EXPORT struct wd_message_declaration;

EXPORT struct wd_session;
EXPORT struct wd_message;

#define INIT_HANDLER waydapt_dll_init

EXPORT void
INIT_HANDLER(int argc, char **argv);

EXPORT enum waydapt_handler_position { HANDLER_EARLY, HANDLER_LATE };

EXPORT enum waydapt_session_handler_type { HANDLER_FOR_INIT, HANDLER_FOR_DESTRUCT };

EXPORT void
waydapt_add_session_handler(int (*handler)(const struct wd_session*),
			    enum waydapt_handler_position pos,
			    enum waydapt_session_handler_type type);

EXPORT enum waydapt_message_type { MESSAGE_IS_EVENT, MESSAGE_IS_REQUEST };

EXPORT void
waydapt_add_message_handler(const char *interface_name, const char *message_name,
			    int (*handler)(struct wd_message*),
			    enum waydapt_handler_position pos,
			    enum waydapt_message_type type);

EXPORT void
wd_message_declaration_add_handler(struct wd_message_declaration *msg,
				   int (*handler)(struct wd_message*),
				   enum waydapt_handler_position pos);

// add a handler to every message in the protocol:
EXPORT void
waydapt_add_global_message_handler(int (*handler)(struct wd_message*),
				   enum waydapt_handler_position pos);

//--------------------------------------------------------------------------------

// true in the default (no -c) client sessions are threads mode:
EXPORT bool
waydapt_is_multithreaded(void);

// Will be true when called from an INIT_HANDLER, or if waydapt_is_multithreaded:
EXPORT bool
waydapt_in_parent_process(void);

EXPORT const struct wd_interface *
waydapt_protocol_get_interface_by_name(const char *name);

// equivalent to waydapt_protocol_get_interface_by_name("wl_display"):
EXPORT const struct wd_interface *
waydapt_protocol_get_display_interface(void);

EXPORT const struct wd_message_declaration *
wd_interface_get_message_declaration_by_opcode(const struct wd_interface *interface,
					       uint32_t opcode,
					       enum waydapt_message_type type);

EXPORT const struct wd_message_declaration *
wd_interface_get_message_declaration_by_name(const struct wd_interface *interface,
					     const char *name,
					     enum waydapt_message_type type);

EXPORT const char *
wd_interface_get_name(const struct wd_interface *interface);

// The maximum version of the interface, based on the version values in the
// protocol files, combined with the global limits file, using the rules
// described here:
// https://wayland.freedesktop.org/docs/html/ch04.html#sect-Protocol-Versioning
EXPORT uint32_t
wd_interface_get_limited_version(const struct wd_interface *interface);

EXPORT bool
wd_interface_is_global(const struct wd_interface *interface);

EXPORT const struct wd_interface *
wd_session_get_interface_by_object_id(const struct wd_session *session, uint32_t id);

//--------------------------------------------------------------------------------

EXPORT const char *
wd_message_declaration_get_name(const struct wd_message_declaration *msg);

EXPORT const char *
wd_message_declaration_get_signature(const struct wd_message_declaration *msg);

// the strlen of the signature:
EXPORT uint32_t
wd_message_declaration_get_num_args(const struct wd_message_declaration *msg);

// true for events, false for requests:
EXPORT bool
wd_message_declaration_is_event(const struct wd_message_declaration *msg);

EXPORT const struct wd_interface *
wd_message_declaration_get_interface(const struct wd_message_declaration *msg);

// If the message declaration has a new_id argument (there can be at most
// one), returns that argument's interface type, else NULL:
EXPORT const struct wd_interface *
wd_message_declaration_get_new_id_interface(const struct wd_message_declaration *msg);

//--------------------------------------------------------------------------------

// use this in a message handler that needs session info:
EXPORT struct wd_session *
wd_message_get_session(struct wd_message *msg);

// The wd_message_{get_args,put_args,bind_pargs} functions must be called
// based on the message's signature.  There are no type safety checks.  All
// arguments to the message (with preceeding length fields for strings and
// arrays) must appear.  Note for .._put_args and .._bind_pargs: if new memory
// is allocated for strings or arrays, that should be freed by the handler
// after it calls wd_message_next_handler or wd_message_send.
EXPORT void
wd_message_get_args(const struct wd_message *msg, ...);

EXPORT void
wd_message_put_args(struct wd_message *msg, ...);

EXPORT void
wd_message_bind_pargs(struct wd_message *msg, ...);

// Get the single scalar (non array/string) ith argument, which can then be
// cast to the proper type:
EXPORT uint32_t
wd_message_scalar_arg(struct wd_message *msg, int i);

// Get the single string or array ith argument along with its length:
EXPORT void *
wd_message_array_arg(struct wd_message *msg, uint32_t *length, int i);

EXPORT const struct wd_message_declaration *
wd_message_get_declaration(const struct wd_message *msg);

EXPORT uint32_t
wd_message_get_target_object_id(const struct wd_message *msg);

// Most message handlers will call this:
EXPORT int
wd_message_call_next_handler(struct wd_message *msg);

// Message handlers that want to bypass any subsequent handlers can call this
// instead:
EXPORT int
wd_message_send(struct wd_message *msg);

//--------------------------------------------------------------------------------

// Get info about the client session:

EXPORT uid_t
wd_session_get_client_uid(const struct wd_session *session);

EXPORT gid_t
wd_session_get_client_gid(const struct wd_session *session);

EXPORT pid_t
wd_session_get_client_pid(const struct wd_session *session);

// The return value is available to the session destruct handler, and is the
// value returned by the last message handler:
EXPORT int
wd_session_return_value(const struct wd_session *session);

// The errno is available to the session destruct handler.  It corresponds to
// the return value, and so only makes sense if the return value was non-0:
EXPORT int
wd_session_errno(const struct wd_session *session);

END_EXTERN_C

#endif
