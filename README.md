Waydapt: Wayland Adapters

A Waydapt adapter (waydapter) proxies a Wayland compositor: it sits in between that compositor and some or all of its clients.  Each
waydapter is both a client and a server.  It creates its own Wayland socket for use by its clients, and attaches to an existing Wayland
compositor through that compositor's socket.  Multiple waydapters can be used at the same time.

Each waydapter is programmable.  Through command-line options, it can load multiple DLLs that use its simple API to intercept Wayland
message traffic.  Individual message handler functions in these DLLs can modify or drop messages.  A waydapter can also be configured
to filter out particular Wayland global interfaces, and/or limit interfaces to specific versions.  Eventually, it will allow new
interfaces to be added that the compositor does not support (as long as those interfaces don't require back-end support).  A waydapter
loads Wayland protocol XML files at initialization time, so there is no need to rebuild it to support protocol additions.

Motivation:
  The standard Wayland philosophy is that desktop behaviors are either part of the compositor or are made available through a protocol
  extension to clients.  If you want to add new behaviors, that means you have to modify a compositor (or request some compositor's
  maintainer to do it for you).  Waydapt adds another way that takes considerably less effort.  Maybe it will allow some of the work
  that goes into creating compositors to be off-loaded.

This project is just starting.  So far, Waydapt builds and runs on Debian 12.

----

Building:

Clone the git repo as usual.

In the top-level directory, initialize with:

\# mkdir build\
\# meson setup build

Then build with:

\# cd build && ninja

The executable will be build/waydapt.  The current set of DLLs will be in build/examples.

The meson default is to build with debug options enabled.  To get an optimized release:

\# mkdir release\
\# meson --buildtype=release setup release\
\# cd release && ninja

The executable will then be release/waydapt, and the DLLs will be in release/examples.

----

Running:

The waydapt command line looks like:

 waydapt options -- dll1 dll1options -- dll2 dll2options -- dll3 ddl3options ...

The dllNoptions are specific to the dlls, and will be passed to each dll through its INIT_HANDLER function (more on that later).

The first set of options, before the first '--', are for waydapt itself.  These are:

-a fd	      : pass an "anti-lock" file descriptor to sync client startup.  The file descriptor
                should have bee locked exclusively (flock --exclusive) prior to being passed.  Waydapt
		will unlock this file descriptor when its display socket is ready to accept clients.
		Clients can be synchronized by the idiom: "flock --shared file true && client ...",
		which waits for the exclusive lock to be removed before launching the client.  Note
		that the "file" in the "flock --shared file true" call is the file that was used to
		create the fd that was passed to waydapt -a.  Don't use the file descriptor form of
		flock with the same fd that was passed in to waydapt -a.
		This style of synchronization works when one needs a modular separation between the
		launch of this waydapter and its clients.  If you intend to launch the clients from
		the same script as the waydapter, use -z to daemonize waydapt instead.

-c	      : fork before starting each client session, so that each is a separate subprocess.
	        The default (without -c) is to create a thread for each client session in the same
		process.  Cannot be used with -t.

-d display    : The name of the Wayland socket that waydapt will create.  This is required.  Clients
   	        should then be started with the environment variable WAYLAND_DISPLAY set to the same
		name.

-e stderrpref : A prefix for a file to use to redirect stderr for each client session.  The filename
   	        will have "-PID.SEQNUM" appended, where PID is the client's PID, and SEQNUM increments
		up from 1 until the resulting file doesn't yet exist.  Very helpful for debugging
		DLLs.

-f	      : force a flush after every message send.  The default is for waydapt to buffer messages
	        for sending until it sees that there are no further messages waiting to be processed,
		or when its internal buffer is full.  Using -f is less performant, but may make some
		situations easier to debug.

-g globals    : the name of the global limits file.  This is required.  The global limits file is a
   	        text file where each line is "wayland-global-interface-name  max-version".  Blank
		lines or comment lines are not currently allowed (maybe later?).  Each Wayland global
		interface name mentioned must be included in a protocol file (see -p and -P).  The
		waydapter will only accept these given global interfaces and max versions by using its
		built-in handlers for wl_registry.global and wl_registry.bind.  This makes it easy
		for a waydapter to filter out whole protocols without needing a DLL: just don't have
		that protocol's global interfaces in the global limits file.

-h	      : print usage and exit

-o output     : dump a human-readable protocol summary along with registered DLL handler functions to
   	        this file prior to setting up the display socket.  A good way to check that the DLL
		handlers were registered properly.

-p protfile   : Specify a single protocol XML file, and:
-P prottree   : Specify a directory tree of protocol XML files (all with a .xml extension will be
   	        included).  Some combination of -p and -P (multiple of each are allowed) is required,
		and the base wayland.xml file must be included somehow.  The waydapter will parse
		all of these files, and will work even if there are mutually exclusive protocols, so
		no effort is required to determine which protocols work with which compositors (but
		I recommend wayland-info for doing that, if you want - also you can use wayland-info
		to help build the global limits file - see gen-globals.bash).

-t secs	      : terminate after last client disconnects, if no further clients connect for secs
   	      	seconds (secs=0 means immediately after last client).

-z	      : daemonize the waydapter after the display socket is set to accept clients.  This makes
	        it easy to synchronize clients with the waydapter if they are launched from the same
		script: just use "waydapt -z ..." without "&" (synchronously), and follow by launching
		each client with "&" (asynchronously).

----

Each DLL should #include "fordlls.h", which contains the entire DLL API.  Each DLL should have at least the function INIT_HANDLER (#defined to waydapt_dll_init) with the signature:

  EXPORT void INIT_HANDLER(int argc, char **argv)

An attept to load a DLL that does not contain such a function into waydapt will signal an error.  The arguments on the waydapt command line that follow each dll until the next '--' or end of the command line are passed to the INIT_HANDLER for that dll at initialization time, before the display socket is made ready for clients.  Each INIT_HANDLER should register session init, session destruct, and message handlers using the API in fordlls.h.  See the examples for how this is done.

The order of DLLs on the waydapt command line, as well as the ways in which each DLL registers handlers, controls the order of handlers for each session and each message.  Waydapt maintains a list of handlers init and destruct handlers that are called at client session initialization and before client session exit (respectively), as well as a list for each message in the protocol.  The functions waydapt_add_session_handler and waydapt_add_message_handler can be used to add new handler at the front (to be called early) or back (to be called late) of each list.  The lists are printed out in call order in the -o output file.

For client session handlers, all init handlers are called in the session thread or subprocess (if -c is used) when a new client connects, and all destruct handlers are called when that client disconnects.  Each session handler is passed a pointer to the wd_session struct that represents that session.  Destruct handlers can use wd_session_return_value and wd_session_errno functions to determine why the client session is shutting down.  Mostly, session handlers are meant to allow for the allocation and corresponding freeing of per-session data (use __thread vars to hold these) for the DLL.

Unlike session handlers, message handlers are passed a pointer to the wd_message struct that holds all data for the message being handled (although they can get the wd_session from that).  Also, each message handler controls whether later ones in the order are called or not, and can modify the wd_message before calling them.  Each message handler has the signature:

  int (*handler)(struct wd_message *msg)

If a message handler returns a non-0 value, the client will be disconnected (and the destruct session
handlers called).  The message handler can also call either wd_message_call_next_handler on the (possibly modified) message, or wd_message_send on the (possibly modified) message.  The first will invoke the next message handler on the list for this message, while the second will send the message without calling subsequent message handlers.  Also, the message handler can just return 0 without calling either of those functions, and the result will be that the message will be dropped (Be careful dropping messages - some are required!  Also, dropping a message that contains a new_id arg will currently cause problems - I have a plan on how to handle that, but have not begun to implement it yet).

Every waydapter currently has 3 built-in message handlers for: wl_registry.global, wl_registery.bind, and wl_display.delete_id.  The first two are used to enforce the restrictions from the global limits file.  The last is used for internal bookkeeping purposes.  These handlers are installed last, and registered early - so they cannot be bypassed by other message handlers on these messages.  These handlers will appear in the -o output file.

Note that a single DLL can appear more than once on the waydapt command line.  If the two (or more) appearances are via the same inode, then they will share the same addresses (use the same __thread and global vars), else not.  Either case probably only makes sense if the command-line options for each instance of that DLL differ, so that the INIT_HANDLER cand do different things.  The only behavior that can be accomplished with multiple instances of the same DLL that could not be accomplished with a single instance is some more complex hander ordering relative to ther DLLs.


Some structs available to handers (from fordlls.h):

struct wd_session
  per-client-session information, such as the client and server connections, the session termination
  result and errno (available to session destruct handlers), the client's uid, gid and pid.

struct wd_interface
  const objects providing protocol interface introspection

struct wd_message_declaration
  const objects providing protocol message introspection, such as the signature.  Every wd_message
  has one wd_message_declaration.

struct wd_message (only for message handlers)
  the data content of the current message being handled

See the examples for more details.

----

To Do:

- more doc

- more examples
  - can waydapt add server-side decorations, separate theming?
  - can waydapt prevent new windows from stealing focus?
  - alternative to safeclip example that doesn't hack mime types (but that hack works so well!)
  - filter keyboard and mouse input to windows, add more menus to windows, etc.
  - can waydapt create and manage workspaces?
  - filter protocol based on client uid/gid/pid

- ability to drop messages that have new_id args (have to supply a stand-in object with the same id)

- ability to register per-object per-DLL content

- ability to originate messages, even outside the uninvolved side's protocol
