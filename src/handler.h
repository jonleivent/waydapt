/*
 * Copyright © 2023 Jonathan Leivent
 *
 * Permission to use, copy, modify, distribute, and sell this software and its
 * documentation for any purpose is hereby granted without fee, provided that
 * the above copyright notice appear in all copies and that both that copyright
 * notice and this permission notice appear in supporting documentation, and
 * that the name of the copyright holders not be used in advertising or
 * publicity pertaining to distribution of the software without specific,
 * written prior permission.  The copyright holders make no representations
 * about the suitability of this software for any purpose.  It is provided "as
 * is" without express or implied warranty.
 *
 * THE COPYRIGHT HOLDERS DISCLAIM ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO
 * EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE,
 * DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THIS SOFTWARE.
 */

#ifndef HANDLER_H
#define HANDLER_H

#include <stdio.h>
#include "fordlls.h"

void
init_waydapt_session_handlers(void);

void
waydapt_handle_init_session(const struct wd_session *session);

void
waydapt_handle_destroy_session(const struct wd_session *session);

void
waydapt_protocol_add_builtin_handlers(void);

void
waydapt_protocol_add_handlers(int argc, char **argv);

void
waydapt_protocol_output_session_handlers(FILE *fp);

void
waydapt_call_init_session_handlers(const struct wd_session *session);

void
waydapt_call_destroy_session_handlers(const struct wd_session *session);

#endif
