/*
 * Copyright © 2023 Jonathan Leivent
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice (including the
 * next paragraph) shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef UTILS_H
#define UTILS_H

#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <sys/socket.h>
#include <sys/un.h>

void *
xmalloc(size_t size);

void *
xrealloc(void *orig, size_t size);

char *
xstrdup(const char *s);

struct msghdr;

ssize_t
wl_os_recvmsg_cloexec(int sockfd, struct msghdr *msg, int flags);

int
wl_os_socket_cloexec(int domain, int type, int protocol);

int
wl_os_socket_peercred(int sockfd, uid_t *uid, gid_t *gid, pid_t *pid);

int
wl_os_accept_cloexec(int sockfd, struct sockaddr *addr, socklen_t *addrlen);

int
wl_os_epoll_create_cloexec(void);

int
Error(const char *format, ...);

void __attribute__((noreturn))
Fatal(const char *format, ...);

static inline uint32_t
div_roundup(uint32_t n, size_t a)
{
	/* The cast to uint64_t is necessary to prevent overflow when rounding
	 * values close to UINT32_MAX. After the division it is again safe to
	 * cast back to uint32_t.
	 */
	return (uint32_t) (((uint64_t) n + (a - 1)) / a);
}

typedef int32_t wl_fixed_t; //  sign.23.8 fixpoint

#define min(x, y) (((x) < (y))?(x):(y))

void
waydapt_output_dll_sym_info(void *sym, FILE *fp);

#endif
