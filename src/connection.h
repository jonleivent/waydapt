/*
 * Copyright © 2023 Jonathan Leivent
 *
 * Permission to use, copy, modify, distribute, and sell this software and its
 * documentation for any purpose is hereby granted without fee, provided that
 * the above copyright notice appear in all copies and that both that copyright
 * notice and this permission notice appear in supporting documentation, and
 * that the name of the copyright holders not be used in advertising or
 * publicity pertaining to distribution of the software without specific,
 * written prior permission.  The copyright holders make no representations
 * about the suitability of this software for any purpose.  It is provided "as
 * is" without express or implied warranty.
 *
 * THE COPYRIGHT HOLDERS DISCLAIM ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO
 * EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE,
 * DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THIS SOFTWARE.
 */

#ifndef CONNECTION_H
#define CONNECTION_H

#ifdef __cplusplus
#define START_EXTERN_C extern "C" {
#define END_EXTERN_C }
#else
#define START_EXTERN_C
#define END_EXTERN_C
#endif

START_EXTERN_C

#include <stddef.h>

// A buffered bi-directional wire connection through a single shared unix stream fd:
struct wl_connection;

// Constructor:
struct wl_connection *wl_connection_create(int fd);

// Destructor (calls free):
void wl_connection_destroy(struct wl_connection *connection);

// Fetch more data from the wire, and returns size of contents (what
// was fetched + what was already there), or <0 with errno set if an
// error occurred:
int wl_connection_read(struct wl_connection *connection);

// Write size bytes of data into the output buffer of connection,
// flushing when necessary.  Returns -1 if an error, else 0:
int wl_connection_write(struct wl_connection *connection, const void *data, size_t size);

// Flush all of the output buffer (and up to MAX_FDS_OUT fds) to the
// wire, completely freeing the output buffer (but maybe not the
// auxiliary buffer).  Returns the amount written, or -1 if there was
// an error:
int wl_connection_flush(struct wl_connection *connection);

// Copy size bytes from the input buffer to data.  This does NOT check if that
// amount of data is available or not, so it always succeeds (what it when
// size bytes are not available is undefined):
void wl_connection_copy(struct wl_connection *connection, void *data, size_t size);

// Copy and consume size bytes from the input buffer to data.  This
// does NOT check if that amount of data is available or not, so it
// always succeeds (what it when size bytes are not available is
// undefined):
void wl_connection_take(struct wl_connection *connection, void *data, size_t size);

// Put a single (open) fd into the output auxiliary buffer, flushing
// when necessary.  Like wl_connection_write, returns -1 if an error,
// else 0:
int wl_connection_put_fd(struct wl_connection *connection, int32_t fd);

// Pop a single (open) fd from the auxiliary input buffer, fetching
// (wl_connection_read) more data from the wire if necessary.  Returns
// the fd, or <0 if an error:
int32_t wl_connection_pop_fd(struct wl_connection *connection);

// Get the fd used by the connection (not to be confused with ...put_fd and
// ...pop_fd, which operate with fds sent over the connection).
int wl_connection_get_fd(const struct wl_connection *connection);

END_EXTERN_C

#endif
