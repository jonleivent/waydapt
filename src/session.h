/*
 * Copyright © 2023 Jonathan Leivent
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice (including the
 * next paragraph) shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef SESSION_H
#define SESSION_H

#include <stdbool.h>
#include <stdint.h>

struct wd_session;

size_t
waydapt_determine_socket_path(char *buf, size_t size, const char *wayland_socket_name);

int
waydapt_client_session(int clientfd);

struct wl_connection *
wd_session_connection_for_fd(struct wd_session *session, int fd);

struct wl_connection *
wd_session_connection_for_side(struct wd_session *session, bool is_server_side);

bool
wd_session_fd_is_for_server(const struct wd_session *session, int fd);

const struct wd_interface *
wd_session_get_interface_by_object_id(const struct wd_session *session, uint32_t id);

int
wd_session_add_id(struct wd_session *session, bool is_server_side,
		  uint32_t id, const struct wd_interface *interface);

void
wd_session_remove_id(struct wd_session *session, bool is_server_side, uint32_t id);

#endif
