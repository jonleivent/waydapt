/*
 * Copyright © 2023 Jonathan Leivent
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice (including the
 * next paragraph) shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef SLLIST_H
#define SLLIST_H

#include "utils.h"

struct wd_sllist_entry {
  void *data;
  struct wd_sllist_entry *next;
};

struct wd_sllist {
  struct wd_sllist_entry *first, **last_next;
};

static inline void
wd_sllist_init(struct wd_sllist *l)
{
  l->first = NULL;
  l->last_next = &l->first;
}

static inline void
wd_sllist_add_first(struct wd_sllist *l, void *data)
{
  struct wd_sllist_entry *e = xmalloc(sizeof *e);
  e->data = data;
  e->next = l->first;
  l->first = e;
  if (e->next == NULL)
    l->last_next = &e->next;
}

static inline void
wd_sllist_add_last(struct wd_sllist *l, void *data)
{
  struct wd_sllist_entry *e = xmalloc(sizeof *e);
  e->data = data;
  e->next = NULL;
  *l->last_next = e;
  l->last_next = &e->next;
}

static inline unsigned int
wd_sllist_length(const struct wd_sllist *l)
{
  unsigned int i = 0;
  for (struct wd_sllist_entry *e = l->first; e; i++, e = e->next);
  return i;
}

#define wd_sllist_for_each(d, l) \
  for (struct wd_sllist_entry *_e = (l).first; _e ? ((d = _e->data), 1) : 0;  _e = _e->next)

#endif
