/*
 * Copyright © 2023 Jonathan Leivent
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice (including the
 * next paragraph) shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/*

Waydapt parses protocol files during process initialization, unlike standard
Wayland compositors and clients which have compiled-in knowledge their
supported protocols.

Also, waydapt includes a "global limits file" (the -g command line option)
that allows for limiting the global interfaces to only those listed, as well
as limiting those global interface's versions.  This is done so that many
different waydapters can share the same set of protocol XML files and still
implement different protocols based on their individual global limits files.

The intention is to allow the user to collect all Wayland protocol XML files
they may use into one or more directories, and have every waydapter instance
parse them all.  Each waydapter then has the potential to understand all of
those protocols.

Then, distinct global limits files can ge given to individual waydapters to
limit their individual protocols.  Hopefully, it is much easier to configure
distinct global limits files per waydapter than it would be to maintain
separate sets of protocol files for each.

[If there are so many protocol XML files that parsing them all is too slow,
then consider pruning down to closer to the set actually needed for each
waydapter.]

The global limits file is a text file where each line is an interface name,
some whitespace, and a maximum version limit (blank lines and comment lines
are not currently allowed).  A waydapter that loads a specific global limits
file will only support the interfaces named in the file, with the maximum
versions given.  This waydapter will intercept "wl_registry.global" events
issued by the compositor and drop any for global interfaces not listed in the
global limits file, and set the version number of the remainder to the maximum
of the compositor advertised version and the global limits file version.

From the user's perspective, the protocol initialization of a waydapter is
composed of the following stages:

1. parse all of the protocol XML files provided in the -p and -P options

2. read in the global limits file (-g), and limit the protocols based on its
   entries

3. load the DLLs on the command line and execute the "waydapt_dll_init"
   function in each

It is the responsibility of each DLL's waydapt_dll_init function to add client
session init/destruct handlers and individual message handlers.

Once all of the above is accomplished, the waydapter's understanding of the
protocols and what handlers to use is frozen.  The waydapter then creates its
socket and waits for clients.

*/
#define _GNU_SOURCE
#include <stddef.h>
#include <errno.h>
#include <limits.h>
#include <string.h>
#include <expat.h>
#include <stdio.h>
#include <dirent.h>

#include "fordlls.h"
#include "protocol.h"
#include "handler.h"
#include "sllist.h"

struct
{
  struct wd_sllist all_protocols_list;
  struct wd_sllist active_protocols_list;
  const struct wd_interface *wl_display_interface;
} waydapt_protocol;

struct wd_protocol
{
  const char *name;
  const char *filename;
  unsigned int line;
  struct wd_sllist owned_interface_list;
  struct wd_sllist external_msgs_list;

  // a wd_protocol is active if it contains globals that are in the limited protocol
  bool is_active_protocol;
};

struct wd_interface
{
  const char *name;
  const char *filename;
  unsigned int line;
  unsigned int parsed_version;
  unsigned int limited_version;

  struct wd_protocol *owning_protocol;
  struct wd_interface *parent_interface;

  struct wd_sllist message_list;

  struct wd_message_declaration **methods, **events;
  unsigned int method_count, event_count;
};

struct wd_message_arg
{
  const char *name;
  const char *filename;
  unsigned int line;
  char type;
  const char *new_interface_name;
  struct wd_message_declaration *owning_message;
};

struct wd_message_declaration
{
  const char *name;
  const char *filename;
  unsigned int line;
  struct wd_interface *owning_interface;
  struct wd_interface *new_id_interface;
  unsigned int since;
  bool is_event;
  bool is_active;
  uint32_t opcode;
  const char *signature;

  struct wd_message_arg *new_id_arg;

  struct wd_sllist arg_list;

  struct wd_sllist handler_list;

};

struct wd_xml_context
{
  XML_Parser parser;
  const char *filename;
  struct wd_protocol *protocol;
  struct wd_interface *interface;
  struct wd_message_declaration *message;
};

static int
strtouint(const char *str)
{
  long int ret;
  char *end;
  int prev_errno = errno;

  errno = 0;
  ret = strtol(str, &end, 10);
  if (errno != 0 || end == str || *end != '\0')
    return -1;

  /* check range */
  if (ret < 0 || ret > INT_MAX) {
    return -1;
  }

  errno = prev_errno;
  return (int)ret;
}

static void
start_element(void *data, const char *element_name, const char **attributes)
{
  struct wd_xml_context *context = data;

  const char *filename = context->filename;
  unsigned int line = XML_GetCurrentLineNumber(context->parser);

  const char *attr, *value;
  const char *name = NULL;
  const char *arg_type = NULL;
  const char *new_interface_name = NULL;
  int version = 0, since = 0;

  while ((attr = *attributes++) != NULL) {
    value = *attributes++;
    if (strcmp(attr, "name") == 0)
      name = value;
    else if (strcmp(attr, "type") == 0)
      arg_type = value;
    else if (strcmp(attr, "interface") == 0)
      new_interface_name = value;
    else if (strcmp(attr, "version") == 0) {
      version = strtouint(value);
      if (version == -1)
	Fatal("Bad version %s at %s:%u", value, filename, line);
    }
    else if (strcmp(attr, "since") == 0) {
      since = strtouint(value);
      if (since == -1)
	Fatal("Bad 'since' %s at %s:%u", value, filename, line);
    }
  }

  if (strcmp(element_name, "arg") == 0) {
    if (name == NULL)
      Fatal("Arg has no name at %s:%u", filename, line);

    if (arg_type == NULL)
      Fatal("Arg %s has no type at %s:%u", name, filename, line);

    struct wd_message_declaration *message = context->message;
    if (message == NULL)
      Fatal("Arg %s has no owning message at %s:%u", name, filename, line);

    struct wd_message_arg *arg = xmalloc(sizeof *arg);
    arg->name = xstrdup(name);
    arg->filename = xstrdup(filename);
    arg->line = line;
    arg->owning_message = message;
    wd_sllist_add_last(&message->arg_list, arg);

    if (strcmp(arg_type, "int") == 0)
      arg->type = 'i';
    else if (strcmp(arg_type, "uint") == 0)
      arg->type = 'u';
    else if (strcmp(arg_type, "fixed") == 0)
      arg->type = 'f';
    else if (strcmp(arg_type, "string") == 0)
      arg->type = 's';
    else if (strcmp(arg_type, "array") == 0)
      arg->type = 'a';
    else if (strcmp(arg_type, "fd") == 0)
      arg->type = 'h';
    else if (strcmp(arg_type, "new_id") == 0) {
      if (message->new_id_arg != NULL)
	Fatal("Multiple new_id arguments for message %s (%s and %s) at %s:%u",
	      message->name, arg->name, message->new_id_arg->name, filename, line);
      message->new_id_arg = arg;

      if (new_interface_name != NULL) {
	arg->new_interface_name = xstrdup(new_interface_name);
	arg->type = 'n';
      }
      else if (strcmp(message->owning_interface->name,"wl_registry") != 0 ||
	       strcmp(message->name,"bind") != 0)
	// only wl_registry.bind is allowed to have a interface-less new_id arg
	Fatal("Missing interface for new_id %s at %s:%u", name, filename, line);
      else
	arg->type = 'N'; // will become 'suu' : interface-name, version, id
    }
    else if (strcmp(arg_type, "object") == 0)
      // we don't store the interface for the object because we don't check
      // those at runtime.  We don't even check if the object exists.
      arg->type = 'o';
    else
      Fatal("Unknown type %s for argument %s at %s:%u", arg_type, name, filename, line);
  }
  else if (strcmp(element_name, "request") == 0 || strcmp(element_name, "event") == 0) {
    if (name == NULL)
      Fatal("Message has no name at %s:%u", filename, line);

    struct wd_message_declaration *msg = xmalloc(sizeof *msg);
    msg->filename = filename;
    msg->line = line;
    msg->name = xstrdup(name);
    msg->since = since;
    wd_sllist_init(&msg->handler_list);

    struct wd_interface *interface = context->interface;
    if (interface == NULL)
      Fatal("Message %s has no owning interface at %s:%u", name, filename, line);

    msg->owning_interface = interface;
    wd_sllist_init(&msg->arg_list);
    wd_sllist_init(&msg->handler_list);

    wd_sllist_add_last(&interface->message_list, msg);
    if (*element_name == 'e') {
      msg->is_event = true;
      interface->event_count++;
    }
    else {
      msg->is_event = false;
      interface->method_count++;
    }

    context->message = msg;
  }
  else if (strcmp(element_name, "interface") == 0) {
     if (name == NULL)
       Fatal("Interface has no name at %s:%u", filename, line);

     struct wd_interface *interface = xmalloc(sizeof *interface);
     interface->name = xstrdup(name);
     interface->filename = xstrdup(filename);
     interface->line = line;
     interface->parsed_version = version;

     struct wd_protocol *protocol = context->protocol;
     if (protocol == NULL)
       Fatal("Interface %s has no owning protocol at %s:%u", name, filename, line);

     wd_sllist_add_last(&protocol->owned_interface_list, interface);
     interface->owning_protocol = protocol;
     wd_sllist_init(&interface->message_list);

     context->interface = interface;
     context->message = NULL;
  }
  else if (strcmp(element_name, "protocol") == 0) {
    if (name == NULL)
      Fatal("Protocol has no name at %s:%u", filename, line);

    struct wd_protocol *protocol = xmalloc(sizeof *protocol);
    protocol->filename = xstrdup(filename);
    protocol->line = line;
    protocol->name = xstrdup(name);

    wd_sllist_init(&protocol->owned_interface_list);
    wd_sllist_init(&protocol->external_msgs_list);

    wd_sllist_add_last(&waydapt_protocol.all_protocols_list, protocol);

    context->protocol = protocol;
    context->interface = NULL;
    context->message = NULL;
  }
}

#define XML_BUFFER_SIZE 4096

void
parse_protocol_file(const char *filename)
{
  struct wd_xml_context context = { XML_ParserCreate(NULL), filename, NULL, NULL, NULL };
  if (context.parser == NULL)
    Fatal("Failed to create XML parser");

  FILE *fp = fopen(filename, "r");
  if (fp == NULL)
    Fatal("Unable to open protocol file %s: %m", filename);

  XML_SetUserData(context.parser, &context);
  XML_SetStartElementHandler(context.parser, start_element);

  int len;
  do {
    void *buf = XML_GetBuffer(context.parser, XML_BUFFER_SIZE);
    len = fread(buf, 1, XML_BUFFER_SIZE, fp);
    if (len < 0)
      Fatal("Cannot read from protocol file %s: %m", filename);

    if (XML_ParseBuffer(context.parser, len, len == 0) == XML_STATUS_ERROR)
      Fatal("XML parsing error in protocol file %s", filename);
  } while (len > 0);

  fclose(fp);
  XML_ParserFree(context.parser);
}

void
parse_protocol_dir(const char *dirname)
{
  char *cwd = get_current_dir_name();
  if (cwd == NULL)
    Fatal("Cannot get_current_dir_name: %m");

  if (chdir(dirname) != 0)
    Fatal("Cannot chdir to %s: %m", dirname);

  DIR *d = opendir(".");
  if (d == NULL)
    Fatal("Cannot open directory %s: %m", dirname);

  struct dirent *dir;
  while ((dir = readdir(d)) != NULL) {
    const char *fname = dir->d_name;
    if (*fname == '.') // a dot file
      continue;

    if (dir->d_type & DT_DIR) { // a subdir
      parse_protocol_dir(fname);
      continue;
    }

    size_t len = strlen(fname);

    if (len > 4 && strcmp(fname + len - 4, ".xml") != 0)
      continue; // not a .xml file

    parse_protocol_file(fname);
  }
  closedir(d);

  if (chdir(cwd) != 0)
    Fatal("Cannot chdir back to %s: %m", cwd);

  free(cwd);
}

static struct wd_interface *
wd_protocol_find_interface_named(struct wd_protocol *protocol, const char *name)
{
  struct wd_interface *interface;
  wd_sllist_for_each(interface, protocol->owned_interface_list)
    if (strcmp(interface->name, name) == 0)
      return interface;

  return NULL;
}

static void
wd_message_declaration_init_signature(struct wd_message_declaration *msg)
{
  // signature is null terminated, so 1 (null) + 2 ('N' arg) + length
  char *p = xmalloc(3 + wd_sllist_length(&msg->arg_list));
  msg->signature = p;

  struct wd_message_arg *arg;
  wd_sllist_for_each(arg, msg->arg_list) {
    if (arg->type == 'N') { // at most one of these
      *p++ = 's'; *p++ = 'u'; *p++ = 'u';
    }
    else
      *p++ = arg->type;
  }
}

static void
wd_interface_postprocess(struct wd_interface *interface)
{
  interface->methods = xmalloc(interface->method_count * sizeof *interface->methods);
  interface->events = xmalloc(interface->event_count * sizeof *interface->events);
  unsigned int ei = 0, mi = 0;

  struct wd_message_declaration *msg;
  wd_sllist_for_each(msg, interface->message_list) {
    wd_message_declaration_init_signature(msg);

    if (msg->is_event) {
      msg->opcode = ei;
      interface->events[ei++] = msg;
    }
    else {
      msg->opcode = mi;
      interface->methods[mi++] = msg;
    }

    // Determine interface parentage and new_id_interfaces.  Every interface
    // is either global or has a single parent interface in the same protocol.
    // The parent interface is that which contains one or more messages with
    // new_id arguments typed to the child interface.  Also, if the interface
    // for a new_id argument is not found in this protocol, put the message on
    // the external msgs list (these will need additional fixup later).
    if (msg->new_id_arg != NULL && msg->new_id_arg->new_interface_name != NULL) {
      struct wd_interface *child_interface = msg->new_id_interface =
	wd_protocol_find_interface_named(interface->owning_protocol,
					 msg->new_id_arg->new_interface_name);

      if (child_interface == NULL)
	// found an external message (one that creates an object in some other protocol)
	wd_sllist_add_last(&interface->owning_protocol->external_msgs_list, msg);
      else if (child_interface->parent_interface != interface) {
	if (child_interface->parent_interface != NULL) {
	  if (strcmp(child_interface->name, "wl_callback") == 0)
	    continue; // wl_callback is a known violation of single parent rule
	  Fatal("Interface %s has at least two parent interfaces %s and %s in %s",
		child_interface->name, child_interface->parent_interface->name,
		interface->name, child_interface->filename);
	}
	else
	  child_interface->parent_interface = interface;
      }
    }
  }
}

// For a single entry in the global limits file, make sure there is exactly
// one interface for the global name, and set its limited_version based on the
// version_limit and its parsed_version.  Also, add its protocol to the active
// protocols list.
static void
process_global_limit(const char *global_name, int version_limit)
{
  struct wd_protocol *found_in_protocol = NULL;
  struct wd_protocol *protocol;
  wd_sllist_for_each(protocol, waydapt_protocol.all_protocols_list) {
    struct wd_interface *interface;
    wd_sllist_for_each(interface, protocol->owned_interface_list) {
      if (interface->parent_interface != NULL) // we only want globals
	continue;
      if (strcmp(global_name, interface->name) == 0) {
	if (found_in_protocol != NULL) // duplicate global interface
	  Fatal("Global interface %s appears in protocols %s and %s",
		global_name, found_in_protocol->name, protocol->name);

	found_in_protocol = protocol;

	if (version_limit > 0)
	  interface->limited_version = min(version_limit, interface->parsed_version);
	else
	  // a 0 or negative version_limit means no limit:
	  interface->limited_version = interface->parsed_version;

	if (!protocol->is_active_protocol) {
	  protocol->is_active_protocol = true;
	  wd_sllist_add_last(&waydapt_protocol.active_protocols_list, protocol);
	}
      }
    }
  }
  if (found_in_protocol == NULL)
    Fatal("Interface %s in global limits file not found among protocols", global_name);
}

static void
process_global_limits_file(const char *filename)
{
  if (filename == NULL)
    Fatal("No global limits file");

  FILE *fp = fopen(filename, "r");
  if (fp == NULL)
    Fatal("Unable to open global limits file %s: %m", filename);

  char *global_name;
  int version_limit;
  errno = 0;
  while (fscanf(fp, "%ms %d\n", &global_name, &version_limit) == 2) {
    process_global_limit(global_name, version_limit);
    free(global_name);
  }
  if (errno != 0)
    Fatal("Error reading global limits file %s: %m", filename);

  fclose(fp);
}

static struct wd_interface *
wd_interface_global_ancestor(struct wd_interface *interface)
{
  if (interface->parent_interface == NULL)
    return interface;

  return wd_interface_global_ancestor(interface->parent_interface);
}

// For each interface in a protocol, set its limiting version based on its
// global ancestor's limited version.  This follows the Wayland protocol
// versioning rules described in:
// https://wayland.freedesktop.org/docs/html/ch04.html#sect-Protocol-Versioning
// We do this primarily so that we can mark some messages (those with since
// attributes that are too high for the active version) as inactive, and not
// bother searching for external interfaces for any of those.  Later, if/when
// we allow waydapt to originate messages, the distinction between active and
// inactive messages will allow waydapt handler dlls to decide which messages
// they can originate.
static void
wd_protocol_propagate_version_limits(struct wd_protocol *protocol)
{
  struct wd_interface *interface;
  wd_sllist_for_each(interface, protocol->owned_interface_list) {
    struct wd_interface *ancestor = wd_interface_global_ancestor(interface);
    // non-global interfaces are never pruned, only limited.  We assume that
    // the versions in the protocol files were correct.
    interface->limited_version = min(interface->parsed_version, ancestor->limited_version);
    // prune messages with too-high since values
    struct wd_message_declaration *msg;
    wd_sllist_for_each(msg, interface->message_list)
      msg->is_active = (msg->since <= interface->limited_version);
  }
}

// External interfaces are interfaces for new_id args of messages where the
// desired interface is not contained within the same protocol as the message.
// These have to be found after we determine which interfaces and messages are
// active.  Like global interfaces, we require that there is no duplication of
// interfaces for the same name.  There may be duplicate interfaces in the
// known protocol, but the wapdapt composer has the responsibility to filter
// (using the global limits file) down to a unique interface for any active
// global or external interface name.
static void
wd_protocol_link_external_interfaces(struct wd_protocol *protocol)
{
  struct wd_message_declaration *msg;
  wd_sllist_for_each(msg, protocol->external_msgs_list) {
    if (msg->is_active) {
      struct wd_protocol *protocol;
      wd_sllist_for_each(protocol, waydapt_protocol.active_protocols_list) {
	struct wd_interface *interface =
	  wd_protocol_find_interface_named(protocol, msg->new_id_arg->new_interface_name);
	if (interface != NULL) {
	  if (msg->new_id_interface != NULL)
	    Fatal("Duplicate interfaces for %s (%s and %s) used in %s.%s arg %s",
		  interface->name, interface->owning_protocol->name,
		  msg->new_id_interface->owning_protocol->name,
		  msg->owning_interface->name, msg->name, msg->new_id_arg->name);
	  msg->new_id_interface = interface;
	}
      }
      if (msg->new_id_interface == NULL)
	Fatal("No external interface named %s for %s.%s arg %s",
	      msg->new_id_arg->new_interface_name, msg->owning_interface->name, msg->name,
	      msg->new_id_arg->name);
    }
  }
}

//------------------------------------------------------------------------------

void
init_waydapt_protocol(void)
{
  wd_sllist_init(&waydapt_protocol.all_protocols_list);
  wd_sllist_init(&waydapt_protocol.active_protocols_list);
}

void
postprocess_waydapt_protocol(const char *globals_file_name)
{
  // 1st post-parse pass:
  // - fix up message opcodes and signatures
  // - create the methods and events vectors for fast message lookup by opcode
  // - set parent_interface links
  // - prune global interface list
  // - populate external msgs list
  struct wd_protocol *protocol;
  wd_sllist_for_each(protocol, waydapt_protocol.all_protocols_list) {
    struct wd_interface *interface;
    wd_sllist_for_each(interface, protocol->owned_interface_list)
      wd_interface_postprocess(interface);
  }

  // 2nd pass: read the global limits file and add protocols to the
  // active_protocols list if they have globals in the globals file, also
  // setting their limited_version values
  process_global_limits_file(globals_file_name);

  // We now only care about active protocols
  // 3rd pass:
  // - Propagate down the limited_versions and determine which messages are active
  // - Find external interfaces for active external messages
  wd_sllist_for_each(protocol, waydapt_protocol.active_protocols_list) {
    wd_protocol_propagate_version_limits(protocol);
    wd_protocol_link_external_interfaces(protocol);
  }

  // From this point on, the protocol remains constant, except for lists of
  // handlers and the wl_display_interface field.

  waydapt_protocol.wl_display_interface =
    waydapt_protocol_get_interface_by_name("wl_display");

  if (waydapt_protocol.wl_display_interface == NULL)
    Fatal("No wl_display interface exists in the protocol");
}

const struct wd_interface *
waydapt_protocol_get_interface_by_name(const char *name)
{
  struct wd_protocol *protocol;
  wd_sllist_for_each(protocol, waydapt_protocol.active_protocols_list) {
    struct wd_interface *interface = wd_protocol_find_interface_named(protocol, name);
    if (interface != NULL)
      return interface;
  }
  return NULL;
}

const struct wd_interface *
waydapt_protocol_get_display_interface(void)
{
  return waydapt_protocol.wl_display_interface;
}

typedef int (*message_handler)(struct wd_message*);

void
waydapt_add_global_message_handler(message_handler handler, enum waydapt_handler_position pos)
{
  struct wd_protocol *protocol;
  wd_sllist_for_each(protocol, waydapt_protocol.active_protocols_list) {
    struct wd_interface *interface;
    wd_sllist_for_each(interface, protocol->owned_interface_list) {
      int i;
      for (i=0; i < interface->method_count; i++)
	wd_message_declaration_add_handler(interface->methods[i], handler, pos);
      for (i=0; i < interface->event_count; i++)
	wd_message_declaration_add_handler(interface->events[i], handler, pos);
    }
  }
}

//------------------------------------------------------------------------------

const struct wd_message_declaration *
wd_interface_get_message_declaration_by_opcode(const struct wd_interface *interface,
					       uint32_t opcode,
					       enum waydapt_message_type type)
{
  if (type == MESSAGE_IS_EVENT) {
     if (opcode >= interface->event_count)
    return NULL;

     return interface->events[opcode];
  }

  if (opcode >= interface->method_count)
    return NULL;

  return interface->methods[opcode];
}

const struct wd_message_declaration *
wd_interface_get_event_declaration_by_name(const struct wd_interface *interface,
					   const char *name)
{
  for (unsigned int i = 0; i < interface->event_count; i++) {
    const struct wd_message_declaration *msg = interface->events[i];
    if (strcmp(name, msg->name) == 0)
      return msg;
  }
  return NULL;
}

const struct wd_message_declaration *
wd_interface_get_request_declaration_by_name(const struct wd_interface *interface,
					     const char *name)
{
  for (unsigned int i = 0; i < interface->method_count; i++) {
    const struct wd_message_declaration *msg = interface->methods[i];
    if (strcmp(name, msg->name) == 0)
      return msg;
  }
  return NULL;
}

const struct wd_message_declaration *
wd_interface_get_message_declaration_by_name(const struct wd_interface *interface,
					     const char *name,
					     enum waydapt_message_type type)
{
  if (type == MESSAGE_IS_EVENT)
    return wd_interface_get_event_declaration_by_name(interface, name);
  else
    return wd_interface_get_request_declaration_by_name(interface, name);
}

const char *
wd_interface_get_name(const struct wd_interface *interface)
{
  return interface->name;
}

bool
wd_interface_is_global(const struct wd_interface *interface)
{
  return interface->parent_interface == NULL;
}

uint32_t
wd_interface_get_limited_version(const struct wd_interface *interface)
{
  return interface->limited_version;
}

//------------------------------------------------------------------------------

const char *
wd_message_declaration_get_name(const struct wd_message_declaration *msg)
{
  return msg->name;
}

const struct wd_interface *
wd_message_declaration_get_interface(const struct wd_message_declaration *msg)
{
  return msg->owning_interface;
}

const char *
wd_message_declaration_get_signature(const struct wd_message_declaration *msg)
{
  return msg->signature;
}

uint32_t
wd_message_declaration_get_num_args(const struct wd_message_declaration *msg)
{
  return strlen(msg->signature);
}

bool
wd_message_declaration_is_event(const struct wd_message_declaration *msg)
{
  return msg->is_event;
}

bool
wd_message_declaration_is_active(const struct wd_message_declaration *msg)
{
  return msg->is_active;
}

// if there is a new_id arg, this returns its interface, else null:
const struct wd_interface *
wd_message_declaration_get_new_id_interface(const struct wd_message_declaration *msg)
{
  return msg->new_id_interface;
}

struct wd_sllist *
wd_message_declaration_get_handler_list(struct wd_message_declaration *msg)
{
  return &msg->handler_list;
}

//--------------------------------------------------------------------------------

static void
wd_message_declaration_output(const struct wd_message_declaration *msg, FILE *fp)
{
  fprintf(fp, "      %s #%u %s(%s)",
	  msg->is_event ? "event" : "request", msg->opcode, msg->name, msg->signature);
  if (msg->new_id_interface)
    fprintf(fp, "  n:%s", msg->new_id_interface->name);
  fprintf(fp, "\n");
  void *handler;
  wd_sllist_for_each(handler, msg->handler_list)
    waydapt_output_dll_sym_info(handler, fp);
}

static void
wd_interface_output(const struct wd_interface *interface, FILE *fp)
{
  fprintf(fp, "    %s, version %u", interface->name, interface->limited_version);
  if (interface->parent_interface)
    fprintf(fp, " [parent: %s]\n", interface->parent_interface->name);
  else
    fprintf(fp, " ** global **\n");
  const struct wd_message_declaration *msg;
  wd_sllist_for_each(msg, interface->message_list) {
    if (msg->is_active)
      wd_message_declaration_output(msg, fp);
  }
}

void
waydapt_output_protocol(const char *filename)
{
  FILE *fp = fopen(filename, "w");
  if (fp == NULL)
    Fatal("Unable to open protocol output file %s: %m", filename);

  waydapt_protocol_output_session_handlers(fp);

  const struct wd_protocol *protocol;
  wd_sllist_for_each(protocol, waydapt_protocol.active_protocols_list) {
    fprintf(fp, "Protocol %s [%s:%u]\n  Interfaces:\n",
	    protocol->name, protocol->filename, protocol->line);
    const struct wd_interface *interface;
    wd_sllist_for_each(interface, protocol->owned_interface_list) {
      wd_interface_output(interface, fp);
      fprintf(fp, "\n");
    }
  }
  fclose(fp);
}

/* Local Variables:       */
/* mode: c                */
/* eval: (font-lock-add-keywords nil '(("\\<wd_sllist_for_each\\>" . font-lock-keyword-face))) */
/* End:                   */
