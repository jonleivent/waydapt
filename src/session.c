/*
 * Copyright © 2023 Jonathan Leivent
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice (including the
 * next paragraph) shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <sys/epoll.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>

#include "session.h"
#include "connection.h"
#include "map.h"
#include "utils.h"
#include "message.h"
#include "fordlls.h"
#include "handler.h"

struct wd_session {
  struct wl_connection *client_connection;
  struct wl_connection *server_connection;
  struct wd_map *client_id_map;
  struct wd_map *server_id_map;
  int ret, client_errno;
  uid_t client_uid;
  gid_t client_gid;
  pid_t client_pid;
};

int
wd_session_return_value(const struct wd_session *session)
{
  return session->ret;
}

int
wd_session_errno(const struct wd_session *session)
{
  return session->client_errno;
}

uid_t
wd_session_get_client_uid(const struct wd_session *session)
{
  return session->client_uid;
}

gid_t
wd_session_get_client_gid(const struct wd_session *session)
{
  return session->client_gid;
}

pid_t
wd_session_get_client_pid(const struct wd_session *session)
{
  return session->client_pid;
}

bool
wd_session_fd_is_for_server(const struct wd_session *session, int fd)
{
  if (fd == wl_connection_get_fd(session->server_connection))
    return true;
  else if (fd == wl_connection_get_fd(session->client_connection))
    return false;
  Fatal("Bad fd: %d", fd);
}

struct wl_connection *
wd_session_connection_for_side(struct wd_session *session, bool is_server_side)
{
  if (is_server_side)
    return session->server_connection;
  else
    return session->client_connection;
}

struct wl_connection *
wd_session_connection_for_fd(struct wd_session *session, int fd)
{
  if (fd == wl_connection_get_fd(session->server_connection))
    return session->server_connection;
  else if (fd == wl_connection_get_fd(session->client_connection))
    return session->client_connection;
  Fatal("Bad fd %u for session", fd);
}

const struct wd_interface *
wd_session_get_interface_by_object_id(const struct wd_session *session, uint32_t id)
{
  const struct wd_interface *interface = wd_map_lookup(session->client_id_map, id);

  if (interface != NULL)
    return interface;

  return wd_map_lookup(session->server_id_map, id);
}

int
wd_session_add_id(struct wd_session *session, bool is_server_side,
		  uint32_t id, const struct wd_interface *interface)
{
  if (is_server_side)
    return wd_map_add_id(session->server_id_map, id, interface);
  else
    return wd_map_add_id(session->client_id_map, id, interface);
}

void
wd_session_remove_id(struct wd_session *session, bool is_server_side,
		     uint32_t id)
{
  if (is_server_side)
    wd_map_delete_id(session->server_id_map, id);
  else
    wd_map_delete_id(session->client_id_map, id);
}

static int
setup_epoll(int clientfd, int serverfd)
{
  const int epollfd = wl_os_epoll_create_cloexec();
  if (epollfd < 0)
    Fatal("Cannot create epoll fd: %m");

  struct epoll_event ee;
  ee.events = EPOLLIN | EPOLLRDHUP;
  ee.data.ptr = (void*)(intptr_t)clientfd;
  if (epoll_ctl(epollfd, EPOLL_CTL_ADD, clientfd, &ee) != 0)
    Fatal("Can't add client to epoll: %m");

  ee.events = EPOLLIN | EPOLLRDHUP;
  ee.data.ptr = (void*)(intptr_t)serverfd;
  if (epoll_ctl(epollfd, EPOLL_CTL_ADD, serverfd, &ee) != 0)
    Fatal("Can't add client to epoll: %m");

  return epollfd;
}

static int
wd_session_event_loop(struct wd_session *session, int clientfd, int serverfd)
{

  const int epollfd = setup_epoll(clientfd, serverfd);

  int ret = 0;
  for (;;) { // event loop
    struct epoll_event ee;
    ret = epoll_wait(epollfd, &ee, 1, -1);
    if (ret < 0)
      break;

    const int fd = (intptr_t)ee.data.ptr;
    const bool is_server_side = fd == serverfd;
    if (!is_server_side && fd != clientfd)
      Fatal("Bad fd returned from epoll_wait: %d", fd);

    if (ee.events & EPOLLIN) { // we have input
      ret = -1;
      do {
	ret = wd_session_handle_input(session, is_server_side, ret);
      } while (ret > 0);
      if (ret < 0)
	break;

      struct wl_connection *output_conn =
	wd_session_connection_for_side(session, !is_server_side);

      ret = wl_connection_flush(output_conn);
      if (ret < 0)
	break;
    }

    if (ee.events & EPOLLERR) { // error on the input stream
      errno = ECONNABORTED;
      ret = -1;
      break;
    }

    if (ee.events & (EPOLLHUP | EPOLLRDHUP)) { // a hang-up
      if (is_server_side) {
	// A server hang-up usually means we did something wrong, or maybe the
	// user is logging out
	errno = ECONNABORTED;
	ret = -1;
      }
      // a client hang-up is the "normal" way to terminate the connection
      break;
    }
  }

  if (close(epollfd) != 0)
    return -1;
  return ret;
}

size_t
waydapt_determine_socket_path(char *buf, size_t size, const char *wayland_socket_name)
{
  size_t s;
  if (*wayland_socket_name != '/') { // relative pathname
    const char *const xdg_runtime_dir = getenv("XDG_RUNTIME_DIR");
    if (xdg_runtime_dir == NULL)
      Fatal("XDG_RUNTIME_DIR is NULL");
    if (*xdg_runtime_dir != '/')
      Fatal("XDG_RUNTIME_DIR is not absolute: %s", xdg_runtime_dir);

    s = snprintf(buf, size, "%s/%s", xdg_runtime_dir, wayland_socket_name) + 1;
    if (s > size)
      Fatal("Pathname %s/%s is too big (%u > %u bytes)",
	    xdg_runtime_dir, wayland_socket_name, s, size);
  }
  else {
    s = snprintf(buf, size, "%s", wayland_socket_name);
    if (s > size)
      Fatal("Pathname %s is too big (%u > %u bytes)", wayland_socket_name, s, size);
  }
  return s;
}

static int
waydapt_connect_to_socket(const char *wayland_socket_name)
{
  struct sockaddr_un addr;
  addr.sun_family = AF_LOCAL;
  
  if (wayland_socket_name == NULL)
    wayland_socket_name = getenv("WAYLAND_DISPLAY");
  if (wayland_socket_name == NULL)
    wayland_socket_name = "wayland-0";

  size_t name_size =
    waydapt_determine_socket_path(addr.sun_path, sizeof addr.sun_path, wayland_socket_name);
  
  size_t socksize = offsetof(struct sockaddr_un, sun_path) + name_size;

  int fd = wl_os_socket_cloexec(PF_LOCAL, SOCK_STREAM, 0);
  if (fd < 0)
    Fatal("Could not create a socket connection: %m");

  if (connect(fd, (struct sockaddr *) &addr, socksize) < 0)
    Fatal("Could not connect to socket %s: %m", addr.sun_path);

  return fd;
}

extern const char *stderr_prefix;

static void
wd_session_maybe_redirect_stderr(const struct wd_session *session)
{
  if (stderr_prefix == NULL) {
    per_thread_stderr = original_stderr;
    per_thread_STDERR_FILENO = original_STDERR_FILENO;
    return;
  }

  // The suffix of the stderr redirect file will be clientpid.sequence#
  pid_t pid = session->client_pid;

  char buf[256];
  unsigned int i = 0;
  int fd;
  do {
    if (snprintf(buf, sizeof buf, "%s-%u.%u",
		 stderr_prefix, (unsigned int)pid, ++i) >= sizeof buf)
      Fatal("Pathname for stderr redirect file is too long (>%u): %s-%u.%u",
	    sizeof buf - 1, stderr_prefix, (unsigned int)pid, i);

    fd = open(buf, O_CREAT | O_EXCL | O_WRONLY, S_IRUSR | S_IWUSR);
  } while (fd < 0 && errno == EEXIST);

  if (fd < 0)
    Fatal("Unable to open stderr redirect file %s: %m", buf);

  if (waydapt_in_parent_process()) {
    per_thread_STDERR_FILENO = fd;
    per_thread_stderr = fdopen(fd, "w");
  }
  else {
    if (dup2(fd, STDERR_FILENO) < 0)
      Fatal("dup unable to redirect to stderr: %m");

    close(fd);
  }
}

extern int terminate_wait_after_last_client;

_Atomic unsigned int client_session_count = 0; // increments and decrements
_Atomic unsigned int client_session_total = 0; // only increments

int
waydapt_client_session(int clientfd)
{
  ++client_session_count;
  ++client_session_total;

  const int serverfd = waydapt_connect_to_socket(NULL);
  if (serverfd < 0)
    Fatal("Cannot connect to server socket");

  struct wd_session session = {
    .client_connection = wl_connection_create(clientfd),
    .server_connection = wl_connection_create(serverfd),
    .client_id_map = wd_map_create(false),
    .server_id_map = wd_map_create(true),
    .ret = 0,
    .client_errno = 0
  };

  if (wl_os_socket_peercred(clientfd, &session.client_uid, &session.client_gid,
			    &session.client_pid) != 0)
    Fatal("Cannot peercred client through socket");

  // All clients start with wl_display as object id 1:
  const struct wd_interface *display_interface = waydapt_protocol_get_display_interface();
  if (display_interface == NULL)
    Fatal("No wl_display interface exists in the known protocol");

  if (wd_map_add_id(session.client_id_map, 1, display_interface) != 0)
     Fatal("Client side can't add wl_display object 1");

  wd_session_maybe_redirect_stderr(&session);

  // allow dlls to do per-session initialization, if they want:
  waydapt_call_init_session_handlers(&session);

  errno = 0;
  session.ret = wd_session_event_loop(&session, clientfd, serverfd);
  session.client_errno = errno;

  // allow dlls to do per-session teardown, if they need to:
  waydapt_call_destroy_session_handlers(&session);

  wl_connection_destroy(session.client_connection);
  wl_connection_destroy(session.server_connection);
  wd_map_destroy(session.client_id_map);
  wd_map_destroy(session.server_id_map);

  if (--client_session_count == 0 && terminate_wait_after_last_client >= 0) {
    unsigned int saved_client_session_total = client_session_total;
    sleep(terminate_wait_after_last_client);
    // check that there were no new clients while we were sleeping:
    if (saved_client_session_total == client_session_total)
      exit(session.client_errno);
  }

  if (session.ret < 0) {
    errno = session.client_errno;
    return Error("Waydapt event loop error: %m");
  }

  return 0;
}
