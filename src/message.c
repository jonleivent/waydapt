/*
 * Copyright © 2023 Jonathan Leivent
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice (including the
 * next paragraph) shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdarg.h>
#include <string.h>
#include <time.h>

#include "fordlls.h"
#include "map.h"
#include "connection.h"
#include "utils.h"
#include "sllist.h"
#include "session.h"
#include "protocol.h"

struct wd_arg {
  union {
    uint32_t u;
    int32_t i;
    wl_fixed_t f;
    uint32_t o;
    uint32_t n;
    int32_t h;
    uint32_t size;
  };
  char *data; // for s and a
};

#define WL_CLOSURE_MAX_ARGS 20
#define MAX_MSG_SIZE 4096

struct wd_message {
  uint32_t target_id;
  uint32_t opcode;
  struct wd_session *session;
  const struct wd_message_declaration *decl;

  struct wd_sllist_entry *handlers;

  struct wd_arg argv[WL_CLOSURE_MAX_ARGS];
  uint32_t buf[MAX_MSG_SIZE / sizeof(uint32_t)];
};

uint32_t
wd_message_get_target_object_id(const struct wd_message *msg)
{
  return msg->target_id;
}

const struct wd_message_declaration *
wd_message_get_declaration(const struct wd_message *msg)
{
  return msg->decl;
}

struct wd_session *
wd_message_get_session(struct wd_message *msg)
{
  return msg->session;
}

typedef int (*message_handler)(struct wd_message*);

int
wd_message_call_next_handler(struct wd_message *msg)
{
  struct wd_sllist_entry *e = msg->handlers;
  if (e == NULL)
    return wd_message_send(msg);

  msg->handlers = e->next;
  return ((message_handler)(e->data))(msg);
}

void
wd_message_get_args(const struct wd_message *msg, ...)
{
  va_list args;
  const struct wd_message_declaration *mdecl = wd_message_get_declaration(msg);
  const char *signature = wd_message_declaration_get_signature(mdecl);
  char c;
  va_start(args, msg);
  for (int i = 0; (c = *signature++) != 0; i++) {
    *va_arg(args, uint32_t*) = msg->argv[i].u;
    if (c == 'a' || c == 's')
      *va_arg(args, char**) = msg->argv[i].data;
  }
  va_end(args);
}

void
wd_message_put_args(struct wd_message *msg, ...)
{
  va_list args;
  const struct wd_message_declaration *mdecl = wd_message_get_declaration(msg);
  const char *signature = wd_message_declaration_get_signature(mdecl);
  char c;
  va_start(args, msg);
  for (int i = 0; (c = *signature++) != 0; i++) {
    msg->argv[i].u = va_arg(args, uint32_t);
    if (c == 'a' || c == 's')
      msg->argv[i].data = va_arg(args, char*);
  }
  va_end(args);
}

void
wd_message_bind_pargs(struct wd_message *msg, ...)
{
  va_list args;
  const struct wd_message_declaration *mdecl = wd_message_get_declaration(msg);
  const char *signature = wd_message_declaration_get_signature(mdecl);
  char c;
  va_start(args, msg);
  for (int i = 0; (c = *signature++) != 0; i++) {
    *va_arg(args, uint32_t**) = &msg->argv[i].u;
    if (c == 'a' || c == 's')
      *va_arg(args, char***) = &msg->argv[i].data;
  }
  va_end(args);
}

uint32_t
wd_message_scalar_arg(struct wd_message *msg, int i)
{
  return msg->argv[i].u;
}

void *
wd_message_array_arg(struct wd_message *msg, uint32_t *length, int i)
{
  if (length != NULL)
    *length = msg->argv[i].size;

  return msg->argv[i].data;
}

static void
wd_message_print(const struct wd_message *msg, bool sending)
{
  // for now, always print to stderr
  struct timespec tp;
  clock_gettime(CLOCK_REALTIME, &tp);
  const unsigned int time = (tp.tv_sec * 1000000L) + (tp.tv_nsec / 1000);
  const struct wd_message_declaration *decl = msg->decl;
  const struct wd_interface *interface = wd_message_declaration_get_interface(decl);
  const char *intname = wd_interface_get_name(interface);
  const char *msgname = wd_message_declaration_get_name(decl);

  fprintf(stderr, "[%7u.%03u] %s%s#%u.%s(",
	  time / 1000, time % 1000,
	  sending ? " -> " : "",
	  intname, msg->target_id, msgname);

  const char *signature = wd_message_declaration_get_signature(decl);
  char c;
  for (int i = 0; (c = *signature++) != 0; i++) {
    if (i > 0)
      fprintf(stderr, ", ");

    switch (c) {
    case 'u':
      fprintf(stderr, "%u", msg->argv[i].u);
      break;
    case 'i':
      fprintf(stderr, "%d", msg->argv[i].i);
      break;
    case 'f':
      /* The magic number 390625 is 1e8 / 256 */
      if (msg->argv[i].f >= 0) {
	fprintf(stderr, "%d.%08d",
		msg->argv[i].f / 256,
		390625 * (msg->argv[i].f % 256));
      } else {
	fprintf(stderr, "-%d.%08d",
		msg->argv[i].f / -256,
		-390625 * (msg->argv[i].f % 256));
      }
      break;
    case 's':
      if (msg->argv[i].data)
	fprintf(stderr, "\"%s\"", msg->argv[i].data);
      else
	fprintf(stderr, "nil");
      break;
    case 'o':
      if (msg->argv[i].o) {
	const struct wd_interface *oint =
	  wd_session_get_interface_by_object_id(msg->session, msg->argv[i].o);
	const char *oiname =
	  oint ? wd_interface_get_name(oint) : "<unknown interface>";
	fprintf(stderr, "%s#%u", oiname, msg->argv[i].o);
      } else
	fprintf(stderr, "nil");
      break;
    case 'n':
      {
	const struct wd_interface *nint =
	  wd_message_declaration_get_new_id_interface(decl);
	fprintf(stderr, "new id %s#",
		nint ? wd_interface_get_name(nint) : "[unknown]");
	if (msg->argv[i].n != 0)
	  fprintf(stderr, "%u", msg->argv[i].n);
	else
	  fprintf(stderr, "nil");
      }
      break;
    case 'a':
      fprintf(stderr, "array[%u]", msg->argv[i].size);
      break;
    case 'h':
      fprintf(stderr, "fd %d", msg->argv[i].h);
      break;
    }
  }

  fprintf(stderr, ")\n");
}

//--------------------------------------------------------------------------------

extern int debug_msgs; // 0 = off, 1 = all, 2 = as server, 3 = as client

// Even though this is technically a wd_session function, it is here with
// wd_message because it must closely mirror wd_message_send
int
wd_session_handle_input(struct wd_session *session, bool from_server, int remain)
{
  struct wd_message msg = { .session = session };
  uint32_t *p = msg.buf;
  struct wl_connection *from_conn = wd_session_connection_for_side(session, from_server);

  const int available_bytes = remain >= 0 ? remain : wl_connection_read(from_conn);
  if (available_bytes < 0)
    return Error("Could not read from the socket");
  if (available_bytes < 2 * sizeof *p) // not even enough to read header
    return 0;

  // copy just the header:
  wl_connection_copy(from_conn, p, 2 * sizeof *p);

  const uint32_t id = msg.target_id = *p++;
  const uint32_t size_opcode = *p++;
  const uint32_t opcode = msg.opcode = size_opcode & 0xffff;
  const uint32_t size = size_opcode >> 16;

  if (available_bytes < size)
    return 0; // wait until there is more to read
  if (size > MAX_MSG_SIZE)
    return Error("Length %u is too big in message from %s",
		 size, from_server ? "server" : "client");

  uint32_t *const pend = p + div_roundup(size, sizeof *p);

  const struct wd_interface *interface =
    wd_session_get_interface_by_object_id(session, id);

  if (interface == NULL)
    return Error("No interface for object id %u in message from %s",
		 id, from_server ? "server" : "client");

  msg.decl = wd_interface_get_message_declaration_by_opcode(interface, opcode,
							    from_server ?
							    MESSAGE_IS_EVENT :
							    MESSAGE_IS_REQUEST);

  if (msg.decl == NULL)
    return Error("No %s corresponds to opcode %f for interface %s",
		 from_server ? "event" : "request", opcode, wd_interface_get_name(interface));

  // take the whole message:
  wl_connection_take(from_conn, msg.buf, size);

  const char *signature = wd_message_declaration_get_signature(msg.decl);

  char c;
  for (int i = 0; (c = *signature++) != 0; i++) {
    if (c == 'h') { // don't read from or incr p
      if ((msg.argv[i].h = wl_connection_pop_fd(from_conn)) >= 0)
	continue;

      return Error("Not enough fds for %s.%s",
		 wd_interface_get_name(interface),
		   wd_message_declaration_get_name(msg.decl));
    }

    if (p >= pend)
      return Error("Message arguments are too big for message %s.%s",
		   wd_interface_get_name(interface),
		   wd_message_declaration_get_name(msg.decl));

    uint32_t u = msg.argv[i].u = *p++; // handles everything but:
    switch (c) {
    case 'n':
      const struct wd_interface *new_id_interface =
	wd_message_declaration_get_new_id_interface(msg.decl);
      if (new_id_interface == NULL)
	return Error("Null new_id interface in %s.%s",
		     wd_interface_get_name(interface),
		     wd_message_declaration_get_name(msg.decl));

      if (wd_session_add_id(session, from_server, u, new_id_interface) == 0)
	break;

      if (u >= WL_SERVER_ID_START && !from_server &&
	  strcmp(wd_message_declaration_get_name(msg.decl),"sync") == 0)
	// it's a wayland-idfix delete_id request masquerading as a sync request
	break;

      return Error("Bad new_id value %u in %s.%s", u,
		   wd_interface_get_name(interface),
		   wd_message_declaration_get_name(msg.decl));

    case 'a':
      if (u == 0) {
	msg.argv[i].data = NULL;
      }
      else {
	msg.argv[i].data = (char*)p;
	const uint32_t len = div_roundup(u, sizeof *p);
	if (p + len >= pend)
	  return Error("Array length %u too big for message %s.%s", u,
		       wd_interface_get_name(interface),
		       wd_message_declaration_get_name(msg.decl));
	p += len;
      }
      break;

    case 's':
      if (u == 0) {
	msg.argv[i].data = NULL;
      } else {
	msg.argv[i].data = (char*)p;
	const uint32_t len = div_roundup(u, sizeof *p);
	if (p + len >= pend)
	  return Error("String length %u too big for message %s.%s", u,
		       wd_interface_get_name(interface),
		       wd_message_declaration_get_name(msg.decl));
	if (strnlen((char*)p, u) != u-1)
	  return Error("Mismatch between string and length %u in %s.%s", u,
		       wd_interface_get_name(interface),
		       wd_message_declaration_get_name(msg.decl));

	p += len;
      }
      break;

    default:
      break;
    }
  }

  switch (debug_msgs) {
  case 1: // all
    wd_message_print(&msg, false);
    break;
  case 2: // as server only
    if (!from_server)
      wd_message_print(&msg, false);
    break;
  case 3: // as client only
    if (from_server)
      wd_message_print(&msg, false);
    break;
  default:
    break;
  }

  const uint32_t used = (char*)p - (char*)msg.buf;
  if (used != size)
    return Error("Message used size %u does not match header length field %u for %s.%s",
		 used, size,
		 wd_interface_get_name(interface),
		 wd_message_declaration_get_name(msg.decl));

  struct wd_message_declaration *decl = // cast away constness
    (struct wd_message_declaration*)(msg.decl);
  msg.handlers = wd_message_declaration_get_handler_list(decl)->first;
  const int res = wd_message_call_next_handler(&msg);
  if (res < 0)
    return res;

  return available_bytes - size;
}

//--------------------------------------------------------------------------------

extern bool flush_every_send;

// also functions as the default handler in wd_message_next_handler.
int
wd_message_send(struct wd_message *msg)
{
  const struct wd_message_declaration *decl = msg->decl;
  const bool from_server = wd_message_declaration_is_event(decl);

  switch (debug_msgs) {
  case 1: // all
    wd_message_print(msg, true);
    break;
  case 2: // as server only
    if (from_server)
      wd_message_print(msg, true);
    break;
  case 3: // as client only
    if (!from_server)
      wd_message_print(msg, true);
    break;
  default:
    break;
  }

  const char *signature = wd_message_declaration_get_signature(decl);
  uint32_t *p = (uint32_t*)msg->buf;
  uint32_t *pstart = p;
  uint32_t *pend = (uint32_t*)(msg->buf + sizeof msg->buf);
  char c;
  uint32_t len;
  struct wl_connection *to_conn = wd_session_connection_for_side(msg->session, !from_server);

  p += 2; // fill in the header later, after we calculate the length

  for (int i = 0; (c = *signature++) != 0; i++) {
    if (c == 'h') {
      if (wl_connection_put_fd(to_conn, msg->argv[i].h) == 0)
	continue;

      return Error("Cannot serialize fd %u - maybe it was closed first?",
		   msg->argv[i].h);
    }

    uint32_t u = *p++ = msg->argv[i].u;
    char *data;
    switch (c) {
    case 'a':
      if (u == 0)
	break;
      len = div_roundup(u, sizeof *p);
      data = msg->argv[i].data;
      // if data == p, there's no need to write it again
      if (data != (char*)p) {
	if (data == NULL) // then u should have been 0
	  return Error("Array header claims length %u, but array is null", u);

	if (p + len >= pend)
	  return Error("Array (length=%u) makes message too big to send", u);

	memcpy(p, data, u);
      }
      p += len;
      break;

    case 's':
      data = msg->argv[i].data;
      // if data == p, there's no need to write it again.  Even if it got
      // shorter, because the Wayland protocol allows for strings that are
      // shorter than the arg length specifies, as long as the string is
      // properly null terminated.
      if (data != (char*)p) {
	if (data == NULL) {
	  *(p-1) = 0;
	  break;
	}
	// we don't trust u from above, recalculate it:
	size_t maxlen = (pend - p) * sizeof *p;
	u = strnlen(data, maxlen) + 1;
	if (u > maxlen)
	  return Error("String (length=%u) makes message too big to send", u);

	*(p-1) = u;
	memcpy(p, data, u);
      }
      len = div_roundup(u, sizeof *p);
      p += len;
      break;

    default:
      break;
    }
    if (p >= pend && *signature)
      return Error("Message is too big to send");
  }
  len = (p - pstart) * sizeof *p;
  *pstart++ = msg->target_id;
  *pstart = (len << 16) | (msg->opcode & 0xffff);
  wl_connection_write(to_conn, msg->buf, len);
  if (flush_every_send && wl_connection_flush(to_conn) < 0)
    return -1;

  return 0;
}
