/*
 * Copyright © 2023 Jonathan Leivent
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice (including the
 * next paragraph) shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <fcntl.h>
#include <sys/file.h>
#include <pthread.h>
#include <signal.h>

#include "session.h"

// The following must appear before local includes, so that we can pick up the orignal stderr:

// In thread per client session mode, we cannot redirect stderr separately in
// each thread, so instead we redefine stderr and STDERR_FILENO to be
// per_thread variables:
__thread FILE *per_thread_stderr;
FILE *original_stderr;

__thread int per_thread_STDERR_FILENO;
int original_STDERR_FILENO;

static void
init_stderr(void)
{
  per_thread_stderr = original_stderr = stderr;
  per_thread_STDERR_FILENO = original_STDERR_FILENO = STDERR_FILENO;
}

// Now do local includes:
#include "protocol.h"
#include "session.h"
#include "utils.h"
#include "handler.h"

// returns 0 in (grand)child, >0 in parent, <0 for error
static int
waydapt_double_fork(void) {
  // The point here is to prevent zombies, in the case where the parent has no
  // need to wait for the child.  Using a double fork is the most modular, as
  // it doesn't impact other child creation, as would doing something like
  // signal(SIGCHLD,SIG_IGN).
  pid_t pid = fork();

  if (pid == 0) { // child
    pid = fork();
    if (pid == 0)
      return 0; // grandchild
    else if (pid > 0)
      _exit(0); // child with OK grandchild
    else
      _exit(-1); // child with no grandchild
  }
  else if (pid < 0)
    return pid;

  // We are the grandparent: wait for the child so it doesn't become a zombie.
  // Once the child has exited, the grandchild will not become a zombie
  // because it will get re-parented to pid 1
  int status;
  waitpid(pid, &status, 0); // parent
  return status >= 0 ? pid : status;
}

static pid_t parent_pid_if_forking_sessions = 0;

bool
waydapt_is_multithreaded(void)
{
  return parent_pid_if_forking_sessions == 0;
}

bool
waydapt_forks_child_sessions(void)
{
  return parent_pid_if_forking_sessions != 0;
}

bool
waydapt_in_parent_process(void)
{
  return parent_pid_if_forking_sessions == 0 ||
    parent_pid_if_forking_sessions == getpid();
}

static struct sockaddr_un socket_addr = { AF_LOCAL, { 0 } };
static char lock_path[5 + sizeof socket_addr.sun_path] = { 0 };

// an exit handler for cleaning up the socket and lock files:
static void
waydapt_unlink_socket(void)
{
  if (!waydapt_in_parent_process())
    // only for the start process
    return;

  if (socket_addr.sun_path[0])
    unlink(socket_addr.sun_path);

  if (lock_path[0])
    unlink(lock_path);
}

static int
waydapt_lock_socket(void)
{
  sprintf(lock_path, "%s.lock", socket_addr.sun_path);

  int fd = open(lock_path, O_CREAT | O_CLOEXEC | O_RDWR,
		(S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP));

  if (fd < 0)
    Fatal("Cannot create lock file %s", lock_path);

  if (flock(fd, LOCK_EX | LOCK_NB) < 0)
    Fatal("Cannot lock lock file %s", lock_path);

  return fd;
}

static void
waydapt_create_socket(int *psocketfd, int *plockfd, const char *wayland_socket_name)
{
  waydapt_determine_socket_path(socket_addr.sun_path, sizeof socket_addr.sun_path,
				wayland_socket_name);

  *plockfd = waydapt_lock_socket();

  const int socketfd = *psocketfd = wl_os_socket_cloexec(PF_LOCAL, SOCK_STREAM, 0);
  if (socketfd < 0)
    Fatal("Cannot create socket: %m", socket_addr.sun_path);

  // If there is something already there that we can unlink, do so.  We don't
  // care if this unlink fails for any reason, because if there is still
  // something there, the bind below will fail:
  unlink(socket_addr.sun_path);
  signal(SIGINT, exit); // so that the following is called:
  atexit(waydapt_unlink_socket);

  if (bind(socketfd, (struct sockaddr*)&socket_addr, sizeof socket_addr) < 0)
    Fatal("Cannot bind socket to path %s: %m", socket_addr.sun_path);

  if (listen(socketfd, /*backlog queue length*/128) < 0)
    Fatal("Cannot listen on socket %s: %m", socket_addr.sun_path);
}

const char *stderr_prefix = NULL;

void
waydapt_revert_per_thread_stderr(__attribute__((unused)) void *dummy)
{
  if (per_thread_STDERR_FILENO != original_STDERR_FILENO) {
    fclose(per_thread_stderr);
    per_thread_stderr = original_stderr;
    per_thread_STDERR_FILENO = original_STDERR_FILENO;
  }
}

static void *
waydapt_client_session_thread(void *data)
{
  const int clientfd = (intptr_t)data;
  pthread_cleanup_push(waydapt_revert_per_thread_stderr, NULL);

  waydapt_client_session(clientfd);

  pthread_cleanup_pop(1);
  return NULL;
}

static bool daemonize_after_socket_listen = false;

static int
waydapt_accept_clients(const char *wayland_socket_name,
		       int anti_lock_fd)
{
  // create the socket we listen on as a server. There is no need to epoll it
  // because we can just block the current thread while waiting, because other
  // threads or child processes are used for running sessions.

  int socketfd, lockfd;
  waydapt_create_socket(&socketfd, &lockfd, wayland_socket_name);

  // socket can accept clients now - unlock the anti-lock fd (if it exists)
  if (anti_lock_fd >= 0) {
    if (flock(anti_lock_fd, LOCK_UN) != 0)
      Fatal("Failed to unlock anti-lock fd %u: %m", anti_lock_fd);
    close(anti_lock_fd);
  }

  if (daemonize_after_socket_listen) {
    bool ppifs_is_set = parent_pid_if_forking_sessions > 0;

    pid_t pid = fork();
    if (pid > 0) {
      parent_pid_if_forking_sessions = -1; // make sure exit handler isn't called
      _exit(0); // parent exits immediately;
    }
    else if (pid < 0)
      Fatal("Cannot daemonize : %m");

    if (setsid() < 0)
      Fatal("Could not setsid after daemonizing: %m");
    // Now in the child - so refresh this if it was set:
    if (ppifs_is_set)
      parent_pid_if_forking_sessions = getpid();
  }

  for (;;) { // accept client connections loop

    // this next accept call blocks, but that is OK because sessions run
    // either as separate threads or child processes (depending on
    // session_model), so they won't be blocked
    const int clientfd = wl_os_accept_cloexec(socketfd, NULL, NULL);
    if (clientfd < 0)
      Fatal("accept failure on main socket %s: %m", wayland_socket_name);

    // Both multithreaded and subprocess client session models do the same
    // thing, modulo what it takes to be in the same process (don't close fds
    // the new thread doesn't use) vs. a different one (close unused fds)

    if (waydapt_forks_child_sessions()) { // subprocess per client
      // Use a double fork so that we don't have to wait for all of the
      // children to prevent zombies.  We don't care what their exit values
      // are.
      int pid = waydapt_double_fork();
      if (pid < 0)
	Fatal("Failed to create handle-client child process: %m");

      if (pid == 0) { // this block is the child process
	// we don't need the parent's socket and lock fds:
	close(socketfd);
	close(lockfd);

	return waydapt_client_session(clientfd);
      }

      // still in parent process
      // the child process inherited clientfd, so we can close ours:
      close(clientfd);
    }
    else { // thread per client
      pthread_t thread;
      if (pthread_create(&thread, NULL, waydapt_client_session_thread,
			 (void*)(intptr_t)clientfd) != 0)
	Fatal("Failed to create handle-client thread: %m");
      if (pthread_detach(thread) != 0)
	Fatal("Failed to detach thread: %m");
    }
  }

  return 0;
}

//--------------------------------------------------------------------------------

void usage(const char *exe)
{
  fprintf(stderr,
	  "Usage:\n"
	  "%s options -- ...dlls-and-their-options...\n"
	  "options are:\n"
	  "-a fd : anti-lock file descriptor for synching client startup with socket listen\n"
	  "-c : client sessions are child processes instead of threads\n"
	  "-d display : the name of the Wayland display socket to create\n"
	  "-e stderr-file-prefix : remap stderr to a file with this prefix for each client session\n"
	  "-f : force a flush after every message send\n"
	  "-g globals-file : file with allowed global interfaces and their max version\n"
	  "-h : print this help\n"
	  "-o output-file : dump processed protocol and handler info to file\n"
	  "-p protocol : a single protocol XML file (can be used multiple times)\n"
	  "-P protocols : a directory tree of protocol XML files (can be used multiple times)\n"
	  "-t secs : terminate after last client, and no others for secs (can't be used with -c)\n"
	  "-z : daemonize after listening on socket\n"
	  , exe);
  exit(-1);
}

// Note: the "anti-lock fd" is used as a convenient way to delay client
// startup until the waydapter is listening on its socket.  This is done by
// having the caller create a file and lock it exclusively through a fd, and
// pass that fd through the -a argument to waydapt.  Other clients can be
// launched by "flock -s anti-lock-filename true && client args...".  The
// waydapter will complete its initialization, including all protocol parsing
// and DLL handler registration, create its WAYLAND_DISPLAY socket, listen on
// it, and then drop the exclusive lock on the fd.  That will free up the
// clients to attempt to connect at that time, so that they don't attempt to
// connect before the socket is ready.

bool flush_every_send = false;
int terminate_wait_after_last_client = -1;

int debug_msgs = 0;

int
main(int argc, char **argv)
{
  init_stderr();
  init_waydapt_protocol();
  init_waydapt_session_handlers();

  int opt, anti_lock_fd = -1;
  char *endptr;
  char *wayland_socket_name = NULL;
  char *globals_file_name = NULL;
  char *output_protocol_file_name = NULL;

  while ((opt = getopt(argc, argv, "a:cd:e:fg:ho:p:P:t:z")) != -1) {
    switch (opt) {
    case 'a': // anti-lock file descriptor
      if (anti_lock_fd != -1)
	Fatal("multiple -a options are not allowed");
      anti_lock_fd = strtol(optarg, &endptr, 10);
      if (*endptr != 0 || anti_lock_fd < 0)
	Fatal("Bad anti-lock fd: %s", optarg);
      break;
    case 'c': // fork a child process per client switch
      if (terminate_wait_after_last_client >= 0)
	Fatal("-t and -c cannot be used together");
      parent_pid_if_forking_sessions = getpid();
      break;
    case 'd': // wayland display (socket) name/path to use
      if (wayland_socket_name != NULL)
	Fatal("multiple -d options are not allowed");
      wayland_socket_name = optarg;
      break;
    case 'e': // per-client stderr prefix
      if (stderr_prefix != NULL)
	Fatal("multiple -e options are not allowed");
      stderr_prefix = optarg;
      break;
    case 'f': // flush every send
      flush_every_send = true;
      break;
    case 'g': // globals file
      if (globals_file_name != NULL)
	Fatal("multiple -g options are not allowed");
      globals_file_name = optarg;
      break;
    case 'o': // output protocol file
      if (output_protocol_file_name != NULL)
	Fatal("multiple -o options are not allowed");
      output_protocol_file_name = optarg;
      break;
    case 'p': // individual protocol file
      parse_protocol_file(optarg);
      break;
    case 'P': // dir of protocol files
      parse_protocol_dir(optarg);
      break;
    case 't': // terminate with last client
      if (parent_pid_if_forking_sessions > 0)
	Fatal("-t and -c cannot be used together");
      terminate_wait_after_last_client = strtol(optarg, &endptr, 10);
      if (*endptr != 0 || terminate_wait_after_last_client < 0)
	Fatal("Bad -t delay: %s", optarg);
      break;
    case 'z': // daemonize after listening on socket
      daemonize_after_socket_listen = true;
      break;
    default:
      usage(argv[0]);
    }
  }

  // adjust to get past above options to dlls
  argc -= optind;
  argv += optind;

  // do all postprocessing
  postprocess_waydapt_protocol(globals_file_name);

  // attach dll handlers based on remaining command line options:
  waydapt_protocol_add_handlers(argc, argv);

  // Now the entire protocol, including handlers, will remain constant.

  if (output_protocol_file_name)
    waydapt_output_protocol(output_protocol_file_name);

  // If WAYLAND_DEBUG is set to 1, msgs will be printed on both sides of the
  // waydapter.  If it is set to server, msgs will only be printed when the
  // waydapter acts as a server (between the waydapter and its clients).  If
  // it is set to clietn, msgs will only be printed when the waydapter acts as
  // a client (between the waydapter and the compositor).
  const char *debug = getenv("WAYLAND_DEBUG");
  if (debug && strstr(debug, "1"))
    debug_msgs = 1;
  else if (debug && strstr(debug, "server"))
    debug_msgs = 2;
  else if (debug && strstr(debug, "client"))
    debug_msgs = 3;

  // Open for business:
  if (wayland_socket_name == NULL)
    wayland_socket_name = "waydapt-0";
  return waydapt_accept_clients(wayland_socket_name, anti_lock_fd);
}
