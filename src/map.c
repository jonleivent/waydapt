/*
 * Copyright © 2023 Jonathan Leivent
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice (including the
 * next paragraph) shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdlib.h>
#include "map.h"
#include "utils.h"

struct wd_map {
  uint32_t count;
  uint32_t size;
  bool is_server_side;
  const struct wd_interface **vect;
};

bool
wd_map_is_server_side(const struct wd_map *map)
{
  return map->is_server_side;
}

struct wd_map *
wd_map_create(bool is_server_side)
{
  struct wd_map *map = xmalloc(sizeof *map);
  map->count = 1; // because we don't use id 0
  map->size = 16 * sizeof map->vect[0];
  map->is_server_side = is_server_side;
  map->vect = xmalloc(map->size);
  return map;
}

const struct wd_interface *
wd_map_lookup(const struct wd_map *map, uint32_t id)
{
  if (map->is_server_side) {
    if (id < WL_SERVER_ID_START)
      return NULL;
    id -= WL_SERVER_ID_START;
  }
  else if (id >= WL_SERVER_ID_START)
    return NULL;

  if (id >= map->count)
    return NULL;

  return (const struct wd_interface*)((uintptr_t)(map->vect[id]) & ~0x3);
}

int
wd_map_add_id(struct wd_map *map, uint32_t id, const struct wd_interface *interface)
{
  // A conneciton side can only insert ids from its side.  The client also
  // cannot insert ids over existing ones - they have to be NULLED first by a
  // separate call, which is what the wl_display::delete_id event does.  We
  // trust the server, not the client, so we let the server insert ids over
  // existing ones without waiting for an ack from the client (which doesn't
  // exist in the protocol anyway).  If we allowed the client to overwrite
  // existing ids, then a rogue client could possibly circumvent whatever this
  // middleware is trying to accomplish by changing the type of an id just
  // before the server sends it an event.
  if (map->is_server_side) {
    if (id < WL_SERVER_ID_START)
      return -1;
    id -= WL_SERVER_ID_START;
  }
  else if (id >= WL_SERVER_ID_START)
    return -1;

  const uint32_t max_allowed = map->count;

  if (id > max_allowed)
    return -1;

  if (id == max_allowed) {
    if (max_allowed * sizeof map->vect[0] >= map->size) {
      map->size *= 2;
      map->vect = xrealloc(map->vect, map->size);
    }
    map->count++;
  }
  else if (map->vect[id] != NULL && // a replacement
	   !map->is_server_side && // of a client-side id
	   ((uintptr_t)(map->vect[id]) & 0x1) == 0) // but no prior delete_id
    return -1;

  map->vect[id] = interface;
  return 0;
}

void
wd_map_delete_id(struct wd_map *map, uint32_t id)
{
  // Only the server sends delete_id events, and only about client ids. Ignore
  // any that don't make sense (the wayland-idfix fork uses id ~0 in a
  // delete_id as a handshake).
  if (id >= map->count)
    return;

  ((uintptr_t*)(map->vect))[id] |= 1;
}

void
wd_map_destroy(struct wd_map *map)
{
  free(map->vect);
  free(map);
}
