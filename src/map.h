/*
 * Copyright © 2023 Jonathan Leivent
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice (including the
 * next paragraph) shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef MAP_H
#define MAP_H

#include <stdbool.h>
#include <stdint.h>

#define WL_SERVER_ID_START 0xff000000

struct wd_interface;
struct wd_map;

struct wd_map *
wd_map_create(bool is_server_side);

const struct wd_interface *
wd_map_lookup(const struct wd_map *map, uint32_t id);

int
wd_map_add_id(struct wd_map *map, uint32_t id, const struct wd_interface *interface);

void
wd_map_delete_id(struct wd_map *map, uint32_t id);

void
wd_map_destroy(struct wd_map *map);

bool
wd_map_is_server_side(const struct wd_map *map);

#endif
