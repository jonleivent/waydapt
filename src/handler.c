/*
 * Copyright © 2023 Jonathan Leivent
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice (including the
 * next paragraph) shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdio.h>
#include <string.h>
#include <dlfcn.h>

#include "fordlls.h"
#include "handler.h"
#include "session.h"
#include "utils.h"
#include "sllist.h"
#include "protocol.h"

#define _STRINGIFY(s) #s
#define STRINGIFY(s) _STRINGIFY(s)
#define INIT_HANDLER_SYM STRINGIFY(INIT_HANDLER)

typedef int (*session_handler)(const struct wd_session*);
typedef int (*message_handler)(struct wd_message*);

struct {
  struct wd_sllist init_list, destruct_list;
} waydapt_session_handlers;

void
init_waydapt_session_handlers(void)
{
  wd_sllist_init(&waydapt_session_handlers.init_list);
  wd_sllist_init(&waydapt_session_handlers.destruct_list);
}

void
waydapt_add_session_handler(session_handler handler,
			    enum waydapt_handler_position pos,
			    enum waydapt_session_handler_type type)
{
  struct wd_sllist *l;
  if (type == HANDLER_FOR_INIT)
    l = &waydapt_session_handlers.init_list;
  else
    l = &waydapt_session_handlers.destruct_list;

  if (pos == HANDLER_EARLY)
    wd_sllist_add_first(l, handler);
  else
    wd_sllist_add_last(l, handler);
}

void
wd_message_declaration_add_handler(struct wd_message_declaration *msg,
				   message_handler handler,
				   enum waydapt_handler_position pos)
{
  struct wd_sllist *l = wd_message_declaration_get_handler_list(msg);
  if (pos == HANDLER_EARLY)
    wd_sllist_add_first(l, handler);
  else
    wd_sllist_add_last(l, handler);
}

// assuming an interface cannot have an event and a request with the same name
void
waydapt_add_message_handler(const char *interface_name, const char *message_name,
			    message_handler handler,
			    enum waydapt_handler_position pos,
			    enum waydapt_message_type type)
{
  const struct wd_interface *interface =
    waydapt_protocol_get_interface_by_name(interface_name);
  if (interface == NULL)
    Fatal("Attempt to add handler for nonexistent interface %s", interface_name);

  const struct wd_message_declaration *msg =
    wd_interface_get_message_declaration_by_name(interface, message_name, type);
  if (msg == NULL)
    Fatal("Attempt to add handler for nonexistent message %s.%s", interface_name, message_name);

  // cast away const on msg
  wd_message_declaration_add_handler((struct wd_message_declaration*)msg, handler, pos);
}

void
waydapt_call_init_session_handlers(const struct wd_session *session)
{
  session_handler h;
  wd_sllist_for_each(h, waydapt_session_handlers.init_list)
    h(session);
}

void
waydapt_call_destroy_session_handlers(const struct wd_session *session)
{
  session_handler h;
  wd_sllist_for_each(h, waydapt_session_handlers.destruct_list)
    h(session);
}

//--------------------------------------------------------------------------------

// We need a built-in handler for wl_registry.global events because we
// (waydapt) may not be aware of all of the protocol that the server
// advertises through wl_registry.global events.  If the server advertises
// some protocol that we don't know about, we can't handle it properly, so we
// must filter it out by dropping the corresponding wl_registry.global events,
// or limiting ones we do know about to the maximum version of that protocol
// we are aware of.
int
builtin_wl_registry_global(struct wd_message *msg)
{
  uint32_t name, advertised_version, len;
  char *interface_name;
  wd_message_get_args(msg, &name, &len, &interface_name, &advertised_version);

  const struct wd_interface *interface =
    waydapt_protocol_get_interface_by_name(interface_name);

  if (interface == NULL)
    // server is advertising an interface that we do not have protocol for
    return 0; // just drop the message with no error

  if (!wd_interface_is_global(interface))
    return Error("wl_registry.global(%s) is not a global interface", interface_name);

  const uint32_t limited_version = wd_interface_get_limited_version(interface);

  if (limited_version == 0)
    // server is advertising an interface that we don't have in the global limits file
    return 0;

  if (advertised_version > limited_version) // limit to max supported version
    wd_message_put_args(msg, name, len, interface_name, limited_version);

  return wd_message_call_next_handler(msg);
}

// We need a built-in handler for wl_registry.bind requests because there is
// no automatic handling of non-typed new_id arguments, which are only allowed
// (or at least only exist) in wl_registry.bind.  Also, we take the
// opportunity to check that the client (which we don't trust) isn't trying to
// bind to a global interface that we don't allow.
int
builtin_wl_registry_bind(struct wd_message *msg)
{
  uint32_t name, version, new_id, len;
  char *interface_name;
  wd_message_get_args(msg, &name, &len, &interface_name, &version, &new_id);

  const struct wd_interface *interface =
    waydapt_protocol_get_interface_by_name(interface_name);

  if (interface == NULL)
    // clients can't bind to unsupported interfaces
    return Error("No interface named %s", interface_name);

  if (!wd_interface_is_global(interface))
    return Error("wl_registry.bind(%s) is not a global interface", interface_name);

  const uint32_t limited_version = wd_interface_get_limited_version(interface);
  if (version > limited_version)
    // clients can't request version above the supported max for an interface
    return Error("Client attempted version %u > %u on interface %s",
		 version, limited_version, interface_name);

  struct wd_session *session = wd_message_get_session(msg);

  if (wd_session_add_id(session, false, new_id, interface) != 0)
    return Error("Bad new_id %u", new_id);

  return wd_message_call_next_handler(msg);
}

// We need a built-in handler for wl_display.delete_id because we do not allow
// clients to reuse client-side object ids unless the server acknowledges
// their deletion (which is what delete_id is for).  This is a security issue,
// mostly, because if the client could reuse an object id and assign it a
// different interface, it might cause the waydapter to pass an event through
// that interface that would otherwise have been filtered.
int
builtin_wl_display_delete_id(struct wd_message *msg)
{
  uint32_t id;
  wd_message_get_args(msg, &id);

  struct wd_session *session = wd_message_get_session(msg);

  wd_session_remove_id(session, false, id);

  return wd_message_call_next_handler(msg);
}

void
waydapt_protocol_add_builtin_handlers(void)
{
  waydapt_add_message_handler("wl_registry", "global", builtin_wl_registry_global,
			      HANDLER_EARLY, MESSAGE_IS_EVENT);
  waydapt_add_message_handler("wl_registry","bind", builtin_wl_registry_bind,
			      HANDLER_EARLY, MESSAGE_IS_REQUEST);
  waydapt_add_message_handler("wl_display", "delete_id", builtin_wl_display_delete_id,
			      HANDLER_EARLY, MESSAGE_IS_EVENT);
}

//--------------------------------------------------------------------------------

#define INIT_HANDLER_SYM STRINGIFY(INIT_HANDLER)

void
waydapt_protocol_add_handlers(int argc, char **argv)
{
  // the (remaining) options will resemble:
  // [--] dll1 -o 1 -p 2 -- dll2 -q 5 -r whatever -- dll3 -s something
  // dlls can be repeated with different args to add handlers in different parts of the dll order

  while (argc > 0) {
    int i;
    for (i = 0; i < argc; i++) { // find end of this segment
      if (strcmp("--", argv[i]) == 0)
	break;
    }

    argv[i] = 0; // null terminate this segment of argv
    if (i > 0) {
      const char *dllname = argv[0];
      void *h = dlopen(dllname, RTLD_LAZY); // maybe RTLD_LAZY?
      if (h == NULL)
	Fatal("Cannot open dll %s: %s", dllname, dlerror());

      void (*dll_init_handler)(int argc, char **argv) = dlsym(h, INIT_HANDLER_SYM);
      if (dll_init_handler == NULL)
	// assume that the dll isn't a proper waydapt dll if it is missing this symbol
	Fatal("Cannot locate %s in dll %s: %s", INIT_HANDLER_SYM, dllname, dlerror());

      dll_init_handler(/*argc=*/i, argv);
    }
    // bypass the used options:
    argc -= i+1;
    argv += i+1;
  }

  // our builtin handlers get final say about going first or last:
  waydapt_protocol_add_builtin_handlers();
}

void
waydapt_protocol_output_session_handlers(FILE *fp)
{

  fprintf(fp, "Session init handlers:\n");
  session_handler h;
  wd_sllist_for_each(h, waydapt_session_handlers.init_list)
    waydapt_output_dll_sym_info(h, fp);

  fprintf(fp, "Session destruct handlers:\n");
  wd_sllist_for_each(h, waydapt_session_handlers.destruct_list)
    waydapt_output_dll_sym_info(h, fp);
}

/* Local Variables:       */
/* mode: c                */
/* eval: (font-lock-add-keywords nil '(("\\<wd_sllist_for_each\\>" . font-lock-keyword-face))) */
/* End:                   */
