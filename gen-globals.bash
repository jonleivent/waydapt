#!/bin/bash

# Generate a global limits file using the wayland-info utility:

wayland-info | sed -ne "s/^\interface:  *'\([^']*\)',  *version:  *\([^,]*\),.*$/\1 \2/p" | sort -u

# You may want to limit the globals further for certain waydapters, such as by
# dropping interfaces meant for privileged utilities.
