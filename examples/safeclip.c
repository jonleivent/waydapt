/*
 * This DLL uses a "mime type hack" to create independent clipboards for use
 * by sandboxed clients.
 *
 * There really still is only one Wayland clipboard.  However, clients of this
 * waydapter will only be able to cut and paste some of the content.  This is
 * done by having the waydapter add a prefix to the mime-types involved in
 * requests and requiring that the same prefix be present on mime-types in
 * events.  Any events that don't have the proper mime-type prefix are
 * dropped.  It also adds the same prefix to each client's window title for
 * convenience.
 *
 * Wayland already has some clipboard security - it doesn't allow a "normal"
 * client to see any clipboard content created by a second client until the
 * first client gets the keyboard focus.  That's certainly much better than
 * Xorg.  But consider how easy it is to accidentally give a client focus.
 * This type of security is insufficient for my sandboxed clients, because it
 * makes "pilot error" too likely.
 *
 * I use this waydapter, together with some scripting around wl-copy and
 * wl-paste, to allow cutting and pasting between sandboxes only when
 * triggered by a compositor menu item.  This makes it much less likely for
 * clipboard content to be shared with clients unintentionally.
 */

#include <stdlib.h>
#include <string.h>

#include "fordlls.h"

__thread char *buffer = NULL;
__thread unsigned int buffer_size = 0;

static void
buffer_at_least(size_t size)
{
  if (buffer_size < size) {
    buffer_size <<= 1;
    buffer_size = (size > buffer_size) ? size : buffer_size;
    free(buffer);
    buffer = malloc(buffer_size);
  }
}

const char *prefix = NULL;
uint32_t prefix_size = 0;

// Note: wd_message_bind_pargs is used below instead of
// wd_message_{get,put}_pargs because this saves us from having to duplicate
// the signature-dispatching conditional to do the put:

int
add_prefix(struct wd_message *msg)
{
  uint32_t *pdontcare;
  const char **pstring;
  uint32_t *plength;
  int32_t *pfd;

  const struct wd_message_declaration *decl = wd_message_get_declaration(msg);
  const char *signature = wd_message_declaration_get_signature(decl);

  // The requests have one of these three signatures:
  if (strcmp(signature, "s")==0)
    wd_message_bind_pargs(msg, &plength, &pstring);
  else if (strcmp(signature, "us")==0)
    wd_message_bind_pargs(msg, &pdontcare, &plength, &pstring);
  else if (strcmp(signature, "sh")==0)
    wd_message_bind_pargs(msg, &plength, &pstring, &pfd);
  else {
    const struct wd_interface *interface = wd_message_declaration_get_interface(decl);
    fprintf(stderr, "Bad signature for add_prefix handler for msg %s.%s: %s",
	    wd_interface_get_name(interface), wd_message_declaration_get_name(decl), signature);
    exit(1);
  }


  buffer_at_least(*plength + prefix_size + 1);

  // write the length and string back into the message:
  *plength = snprintf(buffer, buffer_size, "%s%s", prefix, *pstring) + 1;
  *pstring = buffer;

  return wd_message_call_next_handler(msg);
}

int
remove_prefix_or_drop(struct wd_message *msg)
{
  const char **pstring;
  uint32_t *plength;
  int32_t *pfd;

  const struct wd_message_declaration *decl = wd_message_get_declaration(msg);
  const char *signature = wd_message_declaration_get_signature(decl);

  // The events have one of these two signatures:
  if (strcmp(signature, "s")==0)
    wd_message_bind_pargs(msg, &plength, &pstring);
  else if (strcmp(signature, "sh")==0)
    wd_message_bind_pargs(msg, &plength, &pstring, &pfd);
  else {
    const struct wd_interface *interface = wd_message_declaration_get_interface(decl);
    fprintf(stderr, "Bad signature for remove_prefix_or_drop handler for msg %s.%s: %s",
	    wd_interface_get_name(interface), wd_message_declaration_get_name(decl), signature);
    exit(1);
  }

  // drop the msg if the prefix isn't present:
  if (*plength < prefix_size ||
      (prefix != NULL && strncmp(prefix, *pstring, prefix_size) != 0))
    return 0; // without calling further handlers

  // update the length and string args to bypass the prefix:
  *plength -= prefix_size;
  *pstring += prefix_size;

  return wd_message_call_next_handler(msg);
}

int
clean_up(const struct wd_session *session)
{
  //fprintf(stderr, "safeclip freeing buffer size %u\n", buffer_size);
  free(buffer);
  return 0;
}

void
INIT_HANDLER(int argc, char **argv)
{
  if (argc < 2) {
    fprintf(stderr, "safeclip missing prefix argument");
    exit(1);
  }

  prefix = argv[1];
  prefix_size = strlen(prefix);

  waydapt_add_session_handler(clean_up, HANDLER_LATE, HANDLER_FOR_DESTRUCT);

  // Title requests:

  waydapt_add_message_handler("wl_shell_surface", "set_title", add_prefix,
			      HANDLER_LATE, MESSAGE_IS_REQUEST);

  waydapt_add_message_handler("xdg_toplevel", "set_title", add_prefix,
			      HANDLER_LATE, MESSAGE_IS_REQUEST);

  // All current protocol requests that have a mime-type argument:

  waydapt_add_message_handler("wl_data_offer", "accept", add_prefix,
			      HANDLER_LATE, MESSAGE_IS_REQUEST);

  waydapt_add_message_handler("wl_data_offer", "receive", add_prefix,
			      HANDLER_LATE, MESSAGE_IS_REQUEST);

  waydapt_add_message_handler("wl_data_source", "offer", add_prefix,
			      HANDLER_LATE, MESSAGE_IS_REQUEST);

  waydapt_add_message_handler("zwp_primary_selection_offer_v1", "receive", add_prefix,
			      HANDLER_LATE, MESSAGE_IS_REQUEST);

  waydapt_add_message_handler("zwp_primary_selection_source_v1", "offer", add_prefix,
			      HANDLER_LATE, MESSAGE_IS_REQUEST);

  waydapt_add_message_handler("zwlr_data_control_source_v1", "offer", add_prefix,
			      HANDLER_LATE, MESSAGE_IS_REQUEST);

  waydapt_add_message_handler("zwlr_data_control_offer_v1", "receive", add_prefix,
			      HANDLER_LATE, MESSAGE_IS_REQUEST);

  // All current protocol events that have a mime-type argument:

  waydapt_add_message_handler("wl_data_offer", "offer", remove_prefix_or_drop,
			      HANDLER_LATE, MESSAGE_IS_EVENT);

  waydapt_add_message_handler("wl_data_source", "target", remove_prefix_or_drop,
			      HANDLER_LATE, MESSAGE_IS_EVENT);

  waydapt_add_message_handler("wl_data_source", "send", remove_prefix_or_drop,
			      HANDLER_LATE, MESSAGE_IS_EVENT);

  waydapt_add_message_handler("zwp_primary_selection_offer_v1", "offer", remove_prefix_or_drop,
			      HANDLER_LATE, MESSAGE_IS_EVENT);

  waydapt_add_message_handler("zwp_primary_selection_source_v1", "send", remove_prefix_or_drop,
			      HANDLER_LATE, MESSAGE_IS_EVENT);

  waydapt_add_message_handler("zwlr_data_control_source_v1", "send", remove_prefix_or_drop,
			      HANDLER_LATE, MESSAGE_IS_EVENT);

  waydapt_add_message_handler("zwlr_data_control_offer_v1", "offer", remove_prefix_or_drop,
			      HANDLER_LATE, MESSAGE_IS_EVENT);

}
