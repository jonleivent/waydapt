/*
 * An simple example DLL that adds the client's PID enclosed in braces as a
 * prefix tag to window titles.
 */

#include <stdlib.h>

#include "fordlls.h"

// Note: per-client-session global variables can be declared using __thread,
// which is safe for both threaded and subprocess (-c) client session
// waydapters.
__thread char *buffer = NULL;
__thread size_t buffer_size = 0;

int // <- not static if you want waydapt -o output to see its name
handle_set_title(struct wd_message *msg)
{
  const char *title;
  uint32_t length;

  // Both of the set_title requests in the protocol have the same signature, a
  // single string argument, so they can share this handler without doing any
  // introspection of the signatures.  Note that a single string argument is
  // represented as a length followed by a char vect.  Also note: these
  // wd_message functions that access args are not type safe, and so have to
  // precisely match the signature of the message being handled, otherwise
  // there will be memory access errors.  If you want to make sure, you can do
  // this #if'ed out check first:
#if 0
  const struct wd_message_declaration *decl = wd_message_get_declaration(msg);
  const char *signature = wd_message_declaration_get_signature(decl);
  if (strcmp(signature, "s") != 0) { // a single string argument only!
    fprintf(stderr, "Signature mismatch in handle_set_title: %s\n", signature);
    return -1;
  }
  // Note that the single characters in a signature use the customary Wayland
  // encoding, which is:
  // u : uint32_t
  // i : int32_t
  // f : wl_fixed_t (Wayland's fixed-point value)
  // o : a uint32_t object id
  // n : a uint32_t new object id
  // h : a int32_t file descriptor
  // a : an array (comes with a length field before it)
  // s : a null-terminated string (comes with a length field before it)
#endif
  wd_message_get_args(msg, &length, &title);

  // make sure we have enough buffer space to add the prefix:
  if (buffer_size < length + 32) {
    buffer_size = length + 32;
    free(buffer);
    buffer = malloc(buffer_size);
  }

  // We want the prefix tag to contain the client's PID, which the client
  // session has already fetched from the socket for us:
  const struct wd_session *session = wd_message_get_session(msg);
  unsigned int pid = wd_session_get_client_pid(session);

  // write the PID prefix and title into the buffer:
  length = snprintf(buffer, buffer_size, "{%u}%s",  pid, title) + 1;

  // Write the changes back to the message:
  wd_message_put_args(msg, length, buffer);

  // Continue with any later handlers (wd_message_send is always last in the
  // list):
  return wd_message_call_next_handler(msg);
}

int // <- again, not static if you want -o output to see its name
clean_up(const struct wd_session *session)
{
  free(buffer);
  return 0;
}


void
INIT_HANDLER(int argc, char **argv)
{
  // we don't need any command-line options for something this simple

  // register a cleanup handler for client-session destruction, so that the
  // buffers don't leak as clients come and go:

  waydapt_add_session_handler(clean_up, HANDLER_LATE, HANDLER_FOR_DESTRUCT);

  // If we needed a client-session init handler to create an initial buffer,
  // we could add it with something like (assuming we have a suitable
  // initialize_buffer function):
#if 0
  waydapt_add_session_handler(initialize_buffer, HANDLER_LATE, HANDLER_FOR_INIT);
#endif

  // Register handle_set_title for the set_title request in both the
  // wl_shell_surface and xdg_toplevel interfaces:

  waydapt_add_message_handler("wl_shell_surface", "set_title", handle_set_title,
			      HANDLER_LATE, MESSAGE_IS_REQUEST);

  waydapt_add_message_handler("xdg_toplevel", "set_title", handle_set_title,
			      HANDLER_LATE, MESSAGE_IS_REQUEST);

}
